# Bài tập cuối khóa

> Tổng kết quá trình học tập và rèn luyện. Cơ sở để đánh giá đúng năng lực, khả năng nhận thức và xếp hạng level. 
> Là kết quả đánh giá khách quan cuối cùng cho việc tuyển dụng của bộ phận nhân sự
> Định hướng sự nghiệp, phát triển. Nhận thức được trình độ của cá nhân, ưu điểm và khuyết điểm
> Rèn luyện những tính cách cần có của lập trình viên Frontend

## Requirement

- [ ] Mỗi học viên tham gia khóa đào tạo tự tìm hiểu và đăng ký đề tài. Link đăng ký: https://docs.google.com/spreadsheets/d/1lZRXCwL9ZRQeGPJR6YSezHXw7FgtOK1RxkFfdVTFFsk/edit#gid=1752007856/
- [ ] Các đề tài đăng ký không được trùng nhau, ưu tiên người đăng ký trước
- [ ] Yêu cầu làm việc độc lập, không được thuê hoặc nhờ người khác làm hộ
- [ ] Cuối ngày làm việc yêu cầu tạo merge-request, vi phạm lỗi 2 lần/1 tuần sẽ kết thúc bài tập cuối khóa ngay lập tức
- [ ] Mỗi đề tài yêu cầu tối thiểu có tối thiểu 4 màn hình. Có sử dụng các thư viện cơ bản (xem phần settings)
- [ ] Khi hết thời gian nộp bài, tạo merge-request cuối cùng trước 18h15 ngày 22/4/2022. Nếu tạo sau sẽ không được merge

## Prerequisites

- Đã nắm vững hoặc có kiến thức cơ bản về:
  + HTML/CSS
  + Javascript
  + ReactJS

## Settings

### Yêu cầu sử dụng các thư viện:

- redux
- react-router-dom
- sass
- create-react-app
- axios
- server: json-server

### Các thư viện nâng cao (highly recommend if can use)

- redux-thunk
- redux-toolkit
- react-hook-form
- redux-persist
- typescript
- UI library: https://ant.design/ or https://mui.com/

## Options

 Start server: 

```sh
json-server db.json --watch
```

> The terminal will open localhost with port 3000 as default


Start client: 

```sh
npm start
```

> The terminal will ask to open another port, then select “Y”


