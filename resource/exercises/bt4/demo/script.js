$.fn.jqueryCarousel = function (options) {

  var defaults = {
    id: "",
    autoScroll: false,
    infinite: true,
    scrollRight: true,
    dot: true,
    slideToShow: 1,
    SlideToScroll: 1,
    resHeight: true
  };

  options = $.extend(defaults, options); //parameters

  const actions = (function () {

    console.log('Plugin options ', options)

    return {
      init(el) {
        this.getViewport(el)
      },
      getViewport(el) {
        const wrapper = $("<div/>", { class: "carousel-wrapper" });
        const viewport = $("<div/>", { class: "carousel-viewport" });
        $(viewport)
          .append(this.addIndicator(el))
          .append(this.addBtns(el))
          .append(this.addItems(el))
        $(wrapper).append(viewport)
        $(el).append(wrapper)
      },
      addBtns(el) {
        const btnDiv = $("<div/>", { class: "btn-container" });
        const btnNextDiv = $("<div/>", { class: "button prevBtn", text: "<" });
        const btnPrevDiv = $("<div/>", { class: "button nextBtn", text: ">" });
        return btnDiv.append(btnNextDiv, btnPrevDiv)
      },
      addItems(el) {
        const itemContainer = $("<div/>", { class: "item-container" });
        $(itemContainer).append($(el).find('ul'))
        return itemContainer
      },
      addIndicator(el) {
        const indicatorContainer = $("<div/>", { class: "indicator-container" });
        const ul = $('<ul>');
        for (let i = 0; i < $(el).find('li').length; i++) {
          const li = $('<li>', { class: "dot" });
          $(ul).append(li);
        }
        return indicatorContainer.append(ul)
      },
      adddListener(el) {
        this.onNext(el)
      },
      onNext() {

      }
    }
  })()

  return this.each(function () {
    const el = $(this);
    actions.init(el)
    actions.adddListener(el);
  });

};

$(".carousel").jqueryCarousel({
  autoScroll: false,
  scrollRight: true,
  id: "#sliders-1",
  infinite: true,
  dot: true,
  slideToShow: 1,
  SlideToScroll: 1,
  resHeight: true
});

$(".carousel2").jqueryCarousel({
  autoScroll: false,
  scrollRight: true,
  infinite: true,
  id: "#sliders-2",
  dot: true,
  slideToShow: 1,
  SlideToScroll: 1
});