# Carousel plugin: Requirement
- [ ] Đầu vào là một danh sách list item của carousel
- [ ] Khi ấn button next sẽ animate sang slide phía sau
- [ ] Khi ấn button prev sẽ animate sang slide phía trước
- [ ] Khi ấn vào button indicator sẽ animate đến vị trí của slide đó

# Nâng cao:
- [ ] Có thể swipe/drag các slide
- [ ] Khi slide là đầu tiên hoặc cuối cùng có thể loop về slide tương ứng
- [ ] Responsive khi thay đổi height/width giữa các lần slide, work on mobile

# Tham khảo:
- Javascript Design Pattern
- Jquery Plguin: https://learn.jquery.com/plugins/basic-plugin-creation/