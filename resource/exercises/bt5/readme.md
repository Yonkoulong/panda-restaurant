# Calculator application: Requirement
- [ ] Hiển thị chức năng mày tính
- [ ] Nhấn vào clear thì xóa kết quả và phép tính hiện tại
- [ ] Nhấn vào nút rollback thì hiển thị phép tính lần trước, nếu k có thì disable button. Tối đa 10 phép tính
- [ ] Disable button + - x / % nếu số thứ nhất chưa được nhập hoặc đã nhập số thứ 2