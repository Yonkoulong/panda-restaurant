# Sodoku game application: Requirement
- [ ] Hiển thị màn hình ứng dụng game 9x9
- [ ] Màn hình khởi tạo:
  + [] Hiện button "Play" để khởi tạo ứng dụng
  + [] Hiện random 4 số trong 1 ô vuông 3x3, tối đa 12 số trong 3 ô vuông 3x3
  + [] Những chữ số ban đầu sẽ có màu in đậm
- [ ] 2 nút chức năng là "New Game" và "Back":
  + [] Khi click vào button "New Game": khởi tạo ứng dụng mới
  + [] Khi click vào button "Back": trả về trạng thái trước khi điền số vừa nhập, lưu tối đa 10 lần
- [ ] Khi người chơi điền sai (vd: 2 số giống nhau trên 1 dòng hoặc trong 1 ô vuông 3x3) thì thông báo lỗi với background màu đỏ ở những vị trí giống nhau
- [ ] Khi người chơi hoàn thành thì hiện chữ: "You win"