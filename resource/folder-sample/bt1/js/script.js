// Document ready
$(document).ready(function(){
    $('.bxslider').bxSlider();

    $('.main > .work .content ul li').hover(function(){
    	$(this).children().children('.cover').addClass('in');
    }, function(){
    	$(this).children().children('.cover').removeClass('in');
    });
});