import Form from "../../page/form/form";
import User from "../../page/user/user";
import SignUp from "../../page/signup/signUp";
import React,{useEffect} from 'react';
import Header from "../header/headerComponent";
import { Route, Switch } from 'react-router-dom';  
import { withRouter } from "react-router-dom";

const Main =  () => {
      useEffect(() => {
      }, [])
const FormWithRouter = withRouter(Form);
  return (
    <div>
          <Header />
          <Switch>
                <Route exact path="/">
                      <FormWithRouter />
                </Route>
                <Route exact path={`/user`}>
                        <User />
                </Route>
                <Route exact path="/sign_up">
                        <SignUp />
                </Route>
          </Switch>
    </div>
  )
}
const MainWithRouter = withRouter(Main)
export default MainWithRouter ;
