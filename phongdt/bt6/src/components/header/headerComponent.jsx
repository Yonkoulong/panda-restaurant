import { Link, NavLink } from "react-router-dom";

const Header = (props) => {
      return (
            <div className="header-container">
                  <div className="title-item">
                        <NavLink to="/" className="hook">
                              Home
                        </NavLink>
                  </div>
                  <div className="title-item">
                        <NavLink to="/sign_up" className="hook">
                              SignUp
                        </NavLink>
                  </div>
            </div>
      )
}

export default Header;