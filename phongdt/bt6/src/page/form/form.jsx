import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from "react-router-dom";
import { matchUser } from '../../redux/actions/users.action';

const Form = (props) => {
      const [input, setInput] = useState({account: "", password: ""});
      const [isEmpty, setEmpty] = useState(input.account == "" || input.password == "")
      let [click, setClick] = useState(false);
      const user = useSelector((state) => state.users);
      const dispatch = useDispatch();
      const submitForm = (e) => {
            e.preventDefault()
      }
    
      const handleInput = (e) => {
            setInput({...input, [e.target.name] : e.target.value});
      }
      useEffect(() => {
            const timeOut = setTimeout(() => {
                  return setClick(false)
            }, 2000)
            return (() => {
                  clearTimeout(timeOut())
            });

      }, [])
      const validUser = () => {
            dispatch(matchUser(input.account, input.password))
      }

      const clickLogin = () => {
            if(user.isUser){
                  props.history.push("/user")
            }else{
                  setClick(true)
            }
      }

            return (
                  <div>
                        <div className={'error-message'}>
                                    {isEmpty ? "account or password not empty" : ""}
                              </div>
                        <div className='form-container'>
                              <form onSubmit={submitForm} className="form">
                                    <div className='input-container'>
                                    <div className='label'>
                                          Tài Khoản:
                                    </div>
                                          <input className='input' 
                                                type="text" 
                                                value={input.account} 
                                                onChange ={(e) => {handleInput(e)}}
                                                name="account"
                                          />
                                    </div>
                                    <div className='input-container'>
                                    <div className='label'>
                                          mật khẩu :
                                    </div>
                                          <input className='input' 
                                                type="password" 
                                                value={input.password} 
                                                onChange ={(e) => {handleInput(e)}}
                                                name="password"
                                                onKeyUp={validUser}
                                          />
                                    </div>
                                    <div className="button-container">
                                                <button className = "button" 
                                                      type='submit'
                                                      onClick ={clickLogin}
                                                >login
                                                </button>
                                    </div>
                                    
                              </form>
                        </div>
                  </div>
            )
}

export default Form;
