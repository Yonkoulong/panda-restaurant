import React, { useState , useEffect} from 'react';
import {Link} from "react-router-dom"
import { useSelector, useDispatch } from 'react-redux';
import { matchUser } from '../../redux/actions/users.action';
import { withRouter } from 'react-router-dom';

const Form = (props) => {
      const [input, setInput] = useState({account: "", password: ""});
      const user = useSelector((state) => state.users)
      const dispatch = useDispatch();
      const submitForm = (e) => {
            e.preventDefault()
      }
    
      const handleInput = (e) => {
            setInput({...input, [e.target.name] : e.target.value});
      }
      useEffect(() => {

            return ;
      }, [])
      const login_app = () => {
            return dispatch(matchUser(input.account, input.password))
      }

            return (
                  <div className='form-container'>
                        <form onSubmit={submitForm} className="form">
                              <div className='input-container'>
                                    <input className='input' 
                                          type="text" 
                                          value={input.account} 
                                          onChange ={(e) => {handleInput(e)}}
                                          name="account"
                                    />
                              </div>
                              <div className='input-container'>
                                    <input className='input' 
                                          type="password" 
                                          value={input.password} 
                                          onChange ={(e) => {handleInput(e)}}
                                          name="password"
                                    />
                              </div>
                              <div className="button-container">
                                    <Link to={user.isUser ? "/user" : "/"}>
                                          <button className = "button" 
                                                type='submit'
                                                onClick ={login_app}
                                          >login
                                          </button>
                                    </Link>
                              </div>
                              
                        </form>
                  </div>
            )
}

export default Form;
