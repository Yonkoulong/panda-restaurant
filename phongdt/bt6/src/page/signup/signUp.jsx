import React, {useState, useEffect} from 'react';
import { useDispatch , useSelector} from 'react-redux';
import { Link } from 'react-router-dom';
import { addUser } from '../../redux/actions/users.action';

export default function SignUp(props) {
      const dispatch = useDispatch();
      const [input, setInput] = useState({account: "", password: "", name: "" , dept: "", level: ""});
      const [isEmpty, setIsEmpty] = useState(false);
      const submitForm = (e) => {
            e.preventDefault()
      }

      const validationSignUp = useSelector(state => state.users)
    
      const handleInput = (e) => {
            setInput({...input, [e.target.name] : e.target.value});
      }
      const signUp = () => {
            if(((Object.values(input).filter(val => {return val == ""})).length)  > 0 ){
                setIsEmpty(true);
            }else{
                setIsEmpty(false);
                const payload = {...input, image : "./assets/img/maguire.jpeg"}
                  dispatch(addUser(payload));
                  console.log(validationSignUp.isExist)
                  validationSignUp.isExist ? alert("Tai Khoản đã tồn tại") : alert("bạn đã đăng ký tài khoản thành công vui lòng di chuyển đến trang Login để  đăng nhập lại");
                  return validationSignUp.isExist = false;
            }
      }

      useEffect(() => {}
      , [])
  return (
      <div>
            <div className={isEmpty || validationSignUp ?'error-message' : ""}>
                  {isEmpty? "Field input not empty " : 
                        validationSignUp.isExist ? "account existed" : ""
                  }
            </div>
            <div className='form-container'>
                        <form onSubmit={submitForm} className="form">
                              <div className='input-container'>
                                    <div className='label'>
                                          Tài Khoản :
                                    </div>
                                    <input className='input' 
                                          type="text" 
                                          value={input.account} 
                                          onChange ={(e) => {handleInput(e)}}
                                          name="account"
                                          placeholder='account'
                                    />
                              </div>
                              <div className='input-container'>
                                    <div className='label'>
                                          Mật Khẩu : 
                                    </div>
                                    <input className='input' 
                                          type="password" 
                                          value={input.password} 
                                          onChange ={(e) => {handleInput(e)}}
                                          name="password"
                                          placeholder='password'
                                    />
                              </div>
                              <div className='input-container'>
                                    <div className='label'>
                                          Tên Người Dùng :
                                    </div>
                                    <input className='input' 
                                          type="name" 
                                          value={input.name} 
                                          onChange ={(e) => {handleInput(e)}}
                                          name="name"
                                          placeholder='name'
                                    />
                              </div>
                              <div className='input-container'>
                                    <div className='label'>
                                          Phòng Ban :
                                    </div>
                                    <input className='input' 
                                          type="dept" 
                                          value={input.dept} 
                                          onChange ={(e) => {handleInput(e)}}
                                          name="dept"
                                          placeholder='dept'
                                    />
                              </div>
                              <div className='input-container'>
                                    <div className='label'>
                                          Kinh Nghiệm : 
                                    </div>
                                    <input className='input' 
                                          type="level" 
                                          value={input.level} 
                                          onChange ={(e) => {handleInput(e)}}
                                          name="level"
                                          placeholder='level'
                                    />
                              </div>

                              <div className="button-container">
                                    {/* <Link to="/user"> */}
                                          <button className = "button" 
                                          onClick = {signUp}
                                                >Sign up
                                          </button>
                                    {/* </Link> */}
                              </div>
                              
                        </form>
            </div>
      </div>
  )
}
