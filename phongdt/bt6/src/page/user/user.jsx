import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { logOut } from '../../redux/actions/users.action';

function User() {
      const getUserInStore = useSelector((state) => state.users)
      const user = getUserInStore.user[0];
      const dispatch = useDispatch()
      useEffect(() => {
            return;
      }, [])
      const LogOut = () => {
            return dispatch(logOut())
      }
      const isDisplay = getUserInStore.isUser;
      const userProfile = <div className="user-profile">
            <div className='user-avatar'><img src={user.image ? user.image : "./assets/avatar-1.png "} alt="img" /></div>
            <div className='user-detail'>Tên nhân viên : {user.name}</div>
            <div className='user-detail'>Phòng ban: {user.dept}</div>
            <div className='user-detail'>Trình độ : {user.level}</div>
            <div className='user-button'>
                  <Link to="/">
                        <button onClick={LogOut}>
                              Logout
                        </button>
                  </Link>
            </div>
      </div>
      return isDisplay ? (userProfile) : <div></div>

}
export default User;

