import { combineReducers , createStore, compose, applyMiddleware} from "redux";
import rootReducer from "../reducers/root.reducer";
import thunk from "redux-thunk";
import logger from "redux-logger";
// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleware  = [thunk, logger];
const middlewareEnhancer = applyMiddleware(...middleware);
const configureStore = () => {
      const store = createStore(
            combineReducers(rootReducer),
            middlewareEnhancer
            )
      return store;
}

export default configureStore;
