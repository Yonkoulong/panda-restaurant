import axios from "axios";
import { userUrl } from "../baseUrl/baseUrl";
const saga = () => {
    return axios({
        method: 'get',
        url: userUrl + `users`,
        responseType: 'stream'
      })
}

export default saga;