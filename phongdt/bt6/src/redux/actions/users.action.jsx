import axios from "axios";
const url = "http://localhost:3000/users";
// import { userUrl } from "../baseUrl/baseUrl";

export const ActionTypes = {
      LOGIN_SUCCESS : "LOGIN_SUCCESS",
      LOGIN_FAIL : "LOGIN_FAIL",
      FETCH_API_FAIL : "FETCH_API_FALL",
      LOGOUT : "LOGOUT",
      VALIDATION_SIGN_UP : "VALIDATION_SIGN_UP"
}



export const matchUser = (account, password)  => dispatch =>  {
      return axios.get(url)
            .then((res) => {
                  const getUser = res.data.filter((user) => {
                        return user.account == account && user.password == password;
                  });
                  if(getUser.length !== 0){
                        console.log("oke")
                        return dispatch(login_success(getUser))
                  }else if(account !== "" || password !== ""){
                        console.log("fail")
                        return dispatch(login_fail("account or password incorrect"))
                  }
            })
            .catch((err) => {
                  console.log("not call api")
            })
}
export const login_success = (payload) => {
      return {
            type : ActionTypes.LOGIN_SUCCESS,
            payload : payload
      }
}

export const login_fail = (payload) => {
      return {
            type : ActionTypes.LOGIN_FAIL,
            payload : payload
      }
}

export const fetchApiFail = (payload) => {
      return {
            type : ActionTypes.FETCH_API_FAIL,
            payload : payload
      }
}

export const logOut = () => {
      return {
            type : ActionTypes.LOGOUT
      }
}

export const validationSignUp = (payload) => {
      return {
            type : ActionTypes.VALIDATION_SIGN_UP,
            payload : payload
      }
}

export const addUser = (payload) => dispatch => {
      axios.get(url)
            .then((res) => {
                  let valid = {isExist : true};
                  const accountExist = res.data.filter((user) => {
                              return user.account == payload.account
                  })
                  if (accountExist.length !== 0) {
                        return dispatch(validationSignUp(valid));
                  }else{
                        axios.post(url, payload)
                              .then(() => {
                                    console.log("add success")
                              })
                              .catch((err) => {
                                    console.log(err)
                              })
                  }
            })
            .catch()
      

}
