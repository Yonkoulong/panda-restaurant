import { ActionTypes } from "../actions/users.action";

const initialUser = []
const Users = (state = {isUser : false, error: null, user: null}, action) => {
      
      switch(action.type){
            case ActionTypes.LOGIN_SUCCESS :
                  return {...state, isUser: true, error : null, user : action.payload}
            case ActionTypes.LOGIN_FAIL :
                  return {...state, isUser: false, error : action.payload, user :null}
            case ActionTypes.FETCH_API_FAIL :
                  return {...state, isUser: false, error : action.payload, user :null}
            case ActionTypes.LOGOUT :
                  return {...state}
            case ActionTypes.VALIDATION_SIGN_UP : 
                  return {...state, isExist : action.payload.isExist}
            default:
                  return state;
      }
}

export default Users;