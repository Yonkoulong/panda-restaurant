import './App.scss';
import MainWithRouter from './components/main/mainComponent';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from './redux/store/store';

const store = configureStore()

function App() {
	return (
		<Provider store={store}>
			<BrowserRouter>
				<div className='app' >
					<MainWithRouter/>
				</div>
			</BrowserRouter>
		</Provider>
	);
}

export default App;
