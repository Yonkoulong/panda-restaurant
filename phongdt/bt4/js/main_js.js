jQuery.fn.extend({
     showSlide: function(options) {
          var defaults = {
               autoScroll: false,
               infinite: true,
               scrollRight: true,
               dot: true,
               slideToShow: 1,
               SlideToScroll:1,
               resHeight: true
          };
          options = $.extend(defaults, options);
          let counter = 0;
          let isDragging = false,
          startPos = 0,
          currentTranslate = 0
          const module = (function(){
               return {
                    init(el) {
                         this.getViewport(el);
                    },
                    // get viewport
                    getViewport(el) {
                         const wrapper = $("<div/>", { class: "carousel-wrapper" });
                         const viewport = $("<div/>", { class: "carousel-viewport" });
                         console.log($("<img>", {src: "./assets/icons/icon-controller-left.png", alt: "img"}))
                         $(viewport)
                              .append(this.addIndicator(el))
                              .append(this.addBtn(el))
                              .append(this.addItems(el));
                         $(wrapper).append(viewport);
                         $(el).append(wrapper);
                         this.setStyleIndicator(el);
                         this.cloneItem(el);
                         this.windowLoad(el);
                         this.windowResize(el);
                    },

                    addBtn(el) {
                         const btnDiv = $("<div/>", { class: "btn-container" });
                         const btnNextDiv = $("<div/>", { class: "button prevBtn"});
                         const btnPrevDiv = $("<div/>", { class: "button nextBtn"});
                         const iconPrev = $("<img>", {src: "./assets/icons/icon-controller-right.png", alt: "img"});
                         const iconNext = $("<img>", {src: "./assets/icons/icon-controller-left.png", alt: "img"})
                         return btnDiv.append(btnNextDiv.append(iconNext), btnPrevDiv.append(iconPrev))
                    },

                    addItems(el) {
                         const itemContainer = $("<div/>", { class: "item-container" });
                         $(el).find('ul li').addClass("slide-inner");
                         $(itemContainer).append($(el).find('ul').addClass("slide-ul"))
                         return itemContainer
                    },

                    addIndicator(el) {
                         const indicatorContainer = $("<div/>", { class: "ind-container" });
                         const ul = $('<ul>').addClass("indicator");
                         for (let i = 0; i < $(el).find('li').length; i++) {
                              const li = $('<li>', { class: "dot" });
                              $(ul).append(li);
                         }
                         return indicatorContainer.append(ul)
                    },

                    setStyleIndicator(el){
                         const indBar = el.find(".indicator");
                         const len = el.find(".item-container .slide-inner").length - 2;
                         for(let i =0 ; i <= len; i++){
                              indBar.css("width" , 40*len + "px");
                         }
                    },
                    
                    cloneItem(el){
                         firstItem = el.find(".item-container .slide-inner").filter(':first-child'),
                         lastItem = el.find(".item-container .slide-inner").filter(':last-child'),
                         firstItem.before(lastItem.clone(true)); 
                         lastItem.after(firstItem.clone(true)); 
                    },
                    // event window
                    windowLoad(el){
                         const $this = this;
                         $(window).on("load", () => {
                              let size = el.find(".carousel-viewport").width();
                              $this.responsiveHeight(el);
                              el.find(".slide-ul").css({
                                   "left": "-" + size +"px"
                              }) 
                         })
                    },

                    windowResize(el){
                         const $this = this;
                         $(window).on("resize", () => {
                              let size = el.find(".carousel-viewport").width();
                              $this.transitionSlide(el,false);
                              el.find(".slide-ul").css({
                                   "left": "-" + size +"px"
                              }) 
                              $this.responsiveHeight(el);
                              el.find(".slide-ul .slide-inner img").css({
                                   "width" : size + "px"
                              })
                         })
                    },
                    // listener event
                    addListener(el) {
                         this.addBtnTrigger(el);
                         this.addIndicatorEvent(el);
                         this.addDragEvent(el);
                    },

                    transitionSlide(el,isTransition){
                         let size = el.find(".carousel-viewport").width();
                         el.find(".item-container .slide-ul").css("transition", isTransition? "transform 0.4s linear":"none");
                         el.find(".item-container .slide-ul").css("transform", 'translateX('+ (-size* counter) + 'px)');
                    },

                    responsiveHeight(el){
                         const imgSlide = el.find(`.item-container .slide-inner img`);
                         for(let i = 0; i < imgSlide.length; i++){
                              if(counter == i -1 ){
                                   console.log($(imgSlide[i]).height(),counter,i);
                                   let imgHeight = $(imgSlide[i]).height();
                                   $(imgSlide[i]).css({
                                   "height": options.resHeight?"auto":"500px"
                                   });
                                   el.find(".carousel-viewport").css({
                                        "height" : options.resHeight ? imgHeight:"500" + "px"
                                   })
                                   el.find(".carousel-viewport .item-container").css({
                                        "height" : options.resHeight ? imgHeight:"500" + "px"
                                   })
                              }
                         }
                    },

                    addBtnTrigger(el){
                         const len = el.find(".item-container .slide-inner").length - 2;
                         const $this = this;
                         const prevBtn  = el.find(".btn-container .prevBtn");
                         const nextBtn = el.find(".btn-container .nextBtn");
                         const indicator = el.find(".indicator li");
                         function triggers( changeCounter){
                              counter= changeCounter? counter-=options.SlideToScroll: counter+=options.SlideToScroll;
                              (prevBtn || nextBtn).css("cursor", "grabbing");
                              $this.transitionSlide(el,true);
                              for(let i = 0; i <= indicator.length; i++){
                                   $(indicator[i]).css("backgroundColor", (i == counter)?"red":"transparent");
                              }
                              $this.responsiveHeight(el);
                         }
                         prevBtn.on("click", () =>{
                              if(counter <= -1 ) return $(indicator[0]).css("backgroundColor","red");
                              triggers(true);
                         })
                         nextBtn.on("click", () =>{
                              if(counter >= len ) return $(indicator[0]).css("backgroundColor","red");
                              triggers(false);
                         })
                         // event end transition
                         el.find('.carousel-viewport .item-container').on("transitionend", () =>{
                              for(let i = 0; i <= indicator.length; i++){
                                   counter< 0? counter = len-1: 
                                   0 <= counter && counter <= len-1? this.transitionSlide(el,false):
                                   counter = 0;
                                   $(indicator[i]).css("backgroundColor", (i == counter)?"red":"transparent");
                              }
                         })
                    },

                    addIndicatorEvent(el){
                         const $this = this;
                         const len = el.find(".item-container .slide-inner").length - 2;
                         const indicator = el.find(".indicator li");
                         for(let i =0 ; i <= len; i++){
                              // indicator click and event
                              $(indicator[i]).on("click", () => {
                                   counter = i;
                                   $this.transitionSlide(el,true);
                                   $this.responsiveHeight(el);
                              })
                         }
                    },

                    addDragEvent(el){
                         const $this = this;
                         const carouselItems = el.find(".slide-ul .slide-inner")
                         Array.from(carouselItems).forEach((slide) => {
                              $(slide).find("img").on('dragstart', (e) => e.preventDefault());
                              $(slide).on('touchstart', $this.onTouchStart(el));
                              $(slide).on('touchmove', $this.onTouchMove(el));
                              $(slide).on('touchend', $this.onTouchEnd(el));
                              $(slide).on('mousedown', $this.onTouchStart(el));
                              $(slide).on('mousemove', $this.onTouchMove(el));
                              $(slide).on('mouseup', $this.onTouchEnd(el));
                              $(slide).on('mouseleave', $this.onTouchEnd(el));
                         })
                    },

                    getPositionX(event){
                         console.log(event.type)
                         return event.type.includes('mouse') ? event.pageX : event.originalEvent.touches[0].pageX;
                    },

                    onTouchStart(el){
                         const $this = this;
                         return function(event){
                              startPos = $this.getPositionX(event);
                              isDragging = true;
                              el.find(".carousel-viewport").css("cursor", "grabbing");
                         }
                    },

                    onTouchMove(el){
                         const $this = this;
                         return function(event){
                              if(isDragging){
                                   const currentPosition = $this.getPositionX(event);
                                   currentTranslate = currentPosition - startPos; 
                                   el.find(".carousel-viewport").css("cursor", "grabbing");
                              };
                         }
                    },

                    onTouchEnd(el){
                         const $this = this;
                         const len = el.find(".item-container .slide-inner").length - 2;
                         return function(event){
                              isDragging = false;
                              if(currentTranslate > 100){
                                   counter = counter>0?counter-=options.SlideToScroll:
                                        counter==0&&options.infinite?counter-=options.SlideToScroll:counter;
                              }else if(currentTranslate < -100){
                                   counter = counter<len-1?counter+=options.SlideToScroll:
                                        counter==len-1&&options.infinite?counter+=options.SlideToScroll:counter;
                              }else{
                                   counter = counter;
                              }
                              $this.transitionSlide(el,true);
                              $this.responsiveHeight(el);
                              el.find(".carousel-viewport").css("cursor", "grab")
                              currentTranslate = 0;
                         }
                    }
               }
          })();
          return this.each(function(){
               el = $(this);
               module.init(el);
               module.addListener(el);
          })
     }
});
$("#sliders-1").showSlide({
     autoScroll : true,
     scrollRight: true,
     infinite : true,
     dot: true,
     slideToShow: 1,//do not
     SlideToScroll: 1,
     resHeight: true
});
$("#sliders-2").showSlide({
     autoScroll : false,
     scrollRight: true,
     infinite : true,
     dot: true,
     slideToShow: 1,//do not
     SlideToScroll: 1
});