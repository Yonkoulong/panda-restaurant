import React from 'react'

export default function Header() {
  return (
    <div className='header'>
          <h2 className='app-title'>
                My Calculator
          </h2>
    </div>
  )
}
