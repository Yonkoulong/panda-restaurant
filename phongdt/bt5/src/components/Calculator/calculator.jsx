import React, { Component } from 'react';
import View from './viewComponent';
import Config from './configComponent';

class Calculator extends Component {
      toFixed ;
      constructor(props){
            super(props);
            this.state = {
                  input: [{num : "", isUse : true}, {ope : ""} , {num : "", isUse : true}],
                  result : "",
                  prevData : [],
                  isRollBack : true,
                  alertMessage : '',
                  isDisableOperator : true

            }
      }

      handleClickKey = (value) => {
            const inputs = this.state.input;
            const result = this.state.result;
            const index = inputs.findIndex((input) => {
                  return input.isUse == true
            })
            if(result == ""){
                  if(this.state.result == ""){
                        if(!inputs[index].num.includes(".") ){
                              inputs[index].num += value;
                        }else if(inputs[index].num.includes(".") && !isNaN(parseInt(value)))
                        {
                              inputs[index].num += value;
                        }else{
                              alert("input not a number")
                        }
                  }
                  
                  this.setState((state) => {
                        return state.input = inputs;
                  })
            }else{
                  this.handleClickClear();
                  inputs[0].num = value;
                  this.setState({
                        input : inputs
                  })

            }
            this.handleStatusOp();
      }

      handleStatusOp = () => {
            const inputs = this.state.input;
            let isDisableOperator = this.state.isDisableOperator;
            isDisableOperator = inputs[0].num != "" && inputs[2].num == ""? false : 
                  inputs[0].num != "" && inputs[2].num != ""?true:true;

            this.setState({
                  isDisableOperator : isDisableOperator
            })
            console.log(isDisableOperator);
      }

      handleStatusRollback = () => {
            const len = this.state.prevData.length;
            if(len !== 0 ) {
                  this.setState({
                        isRollBack : false
                  })
            }else{
                  this.setState({
                        isRollBack : false
                  })
            }

      }

      handleClickOperator = (op) => {
            const inputs = this.state.input;
            if(inputs[1].ope == "" && inputs[0].num != ""){
                  inputs[1].ope = op;
                  inputs[0].isUse = false;
            }
            this.setState((state) => {
                  return state.input = inputs;
            })
      }

      handleClickBack = (pre) => {
            const prevData = this.state.prevData;
            const inputs = this.state.input;
            let result = this.state.result;
            const len = prevData.length;
            function setStateRollback(prev){
                  inputs[0].num = prev.num1;
                  inputs[1].ope = prev.ope;
                  inputs[2].num = prev.num2;
                  result = prev.result;
            }
            
            let index = prevData.findIndex((prev, index) => {
                  return prev.result == this.state.result && prev.ope == inputs[1].ope;
            })
            index = this.state.result == ""? len : index;
            if(index != 0) setStateRollback(prevData[index-1]) ;
            this.setState({
                  input : inputs,
                  result : result
            })
      }

      handleClickClear = (cl) => {
            const inputs = this.state.input;
            inputs.map((input,index)=> {
                        input.ope = "";
                  if(index == 0 || index == 2){
                        input.num = '';
                        input.isUse = true;
                  }
            })
            this.setState((state) => {
                  return state.input = inputs ;
            })
            this.setState((state) => {
                  return state.result = "" ;
            })
      }

      handleClickGetResult = () => {
            const operator = this.state.input[1].ope;
            const inputs = this.state.input;
            const result = this.getResult(operator);
            const prevData = this.state.prevData;
            if(result && result !== this.state.result){
                  prevData.push({num1 : inputs[0].num, ope: inputs[1].ope,num2 : inputs[2].num, result : result});
                  const len = prevData.length;
                  this.setState({
                        prevData : len > 10 ? prevData.slice(1, len-1) : prevData
                  })
            }
            this.setState({
                  result : result
            })
            this.handleStatusRollback();
            console.log(this.state.prevData)
      }

      onlyUnique = (value, index, self) => {
            return self.indexOf(value) === index;
      }

      getResult = (ope) => {
            const num1 = parseFloat(this.state.input[0].num);
            const num2 = parseFloat(this.state.input[2].num);
            let result;
            switch (ope){
                  case "x":
                        result = num1 * num2;
                        break;
                  case "/":
                        result = num1 / num2;
                        break;
                  case "+":
                        result = num1 + num2;
                        break;
                  case "-":
                        result = num1 - num2;
                        break;
                  case "%":
                        result = ((num1 / 100) * num2) ;
            }
            // debugger;
            const displayNumber = (number) => {
                  if(number.toString().length < 18) return number;
                  let displayNumber = (number.toString()).substring(0, 20);
                  const errorNum = 0.0000000000000001;
                  displayNumber = ((displayNumber.slice(-4)).includes("999")) ? parseFloat(displayNumber) + errorNum : 
                              ((displayNumber.slice(-1)).includes("1")) ? parseFloat(displayNumber) - errorNum :
                              parseFloat(number)
                              return displayNumber;
            }
            return displayNumber(result);
      }

      render() {
      return (
            <div className='calculator-container'>
                  <View 
                        input = {this.state.input}
                        result = {this.state.result}
                        prevData = {this.state.prevData}
                  />
                  <Config 
                        config = {this.state.config}
                        prevData = {this.state.prevData}
                        handleClickKey = {this.handleClickKey}
                        handleClickOperator = {this.handleClickOperator}
                        handleClickGetResult = {this.handleClickGetResult}
                        handleClickClear = {this.handleClickClear}
                        handleClickBack = {this.handleClickBack}
                        isDisableOperator = {this.state.isDisableOperator}
                        isRollBack = {this.state.isRollBack}
                  /> 
            </div>
      )
      }
}

export default Calculator;