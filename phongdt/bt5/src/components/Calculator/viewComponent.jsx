import React, { Component } from 'react';

class View extends Component {
      constructor(props){
            super(props);
            this.state = {
                  input : this.props.input
            }
      }

      render() {
            const {input} = this.state;
            return (
                  <div className='view-calculator'>
                        <div className='view-input'>
                              <div className='input-item'>{input[0].num}</div>
                              <div className='input-item'>{input[1].ope}</div>
                              <div className='input-item'>{input[2].num}</div>
                        </div>
                        <div className='view-output'>
                              <div className='output-item'>{this.props.result}</div>
                        </div>
                  </div>
            )
      }
}

export default View;