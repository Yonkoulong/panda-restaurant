import React, { Component } from 'react'

class Config extends Component {
      constructor(props){
            super(props);
            this.state = {
                  clear : true,
                  back: true
            }
      }     
      
      handleClickKey = (e) => {
            const value = e.target.value;
            this.props.handleClickKey(value)
      }

      handleClickOperator = (e) => {
            const value = e.target.value;
            this.props.handleClickOperator(value)
      }

      handleClickBack = (e) => {
            const value = e.target.value;
            this.props.handleClickBack(value)
      }

      handleClickClear = (e) => {
            const value = e.target.value;
            this.props.handleClickClear(value)
      }

      handleClickGetResult = (e) => {
            const value = e.target.value;
            this.props.handleClickGetResult(value)
      }
      
      getClass = () => {
            return this.props.isDisableOperator ? "is-disable" : ""
      }

      render() {
      return (
            <div className='cal-key-container'>
                  <div className="cal-key-item">
                        <div className="cal-button"><button onClick={this.handleClickClear} value ={this.state.clear}>Clear</button></div>
                        <div className="cal-button"><button onClick={this.handleClickBack} className={this.props.isRollBack ? "is-disable" : ""} value ={this.state.back}>Back</button></div>
                        <div className="cal-button"><button onClick={this.handleClickOperator} className = {this.getClass()} value ="%">%</button></div>
                        <div className="cal-button"><button onClick={this.handleClickOperator} className = {this.getClass()} value ="/">/</button></div>
                  </div>
                  <div className="cal-key-item">
                        <div className="cal-button"><button onClick={this.handleClickKey} value ={7}>7</button></div>
                        <div className="cal-button"><button onClick={this.handleClickKey} value ={8}>8</button></div>
                        <div className="cal-button"><button onClick={this.handleClickKey} value ={9}>9</button></div>
                        <div className="cal-button"><button onClick={this.handleClickOperator} className = {this.getClass()} value ="x">x</button></div>
                  </div>
                  <div className="cal-key-item">
                        <div className="cal-button"><button onClick={this.handleClickKey} value ={4}>4</button></div>
                        <div className="cal-button"><button onClick={this.handleClickKey} value ={5}>5</button></div>
                        <div className="cal-button"><button onClick={this.handleClickKey} value ={6}>6</button></div>
                        <div className="cal-button"><button onClick={this.handleClickOperator} className = {this.getClass()} value ="-">-</button></div>
                  </div>
                  <div className="cal-key-item">
                        <div className="cal-button"><button onClick={this.handleClickKey} value ={1}>1</button></div>
                        <div className="cal-button"><button onClick={this.handleClickKey} value ={2}>2</button></div>
                        <div className="cal-button"><button onClick={this.handleClickKey} value ={3}>3</button></div>
                        <div className="cal-button"><button onClick={this.handleClickOperator} className = {this.getClass()} value ="+">+</button></div>
                  </div>
                  <div className="cal-key-item">
                        <div className="cal-button cal-zero"><button onClick={this.handleClickKey} value={0}>0</button></div>
                        <div className="cal-button"><button onClick={this.handleClickKey} value=".">.</button></div>
                        <div className="cal-button"><button onClick={this.handleClickGetResult} value="=">=</button></div>
                  </div>
            </div>
      )
      }
}

export default Config;