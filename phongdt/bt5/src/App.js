import './App.scss';
import Calculator from './components/Calculator/calculator';
import Footer from './components/footer/footerComponent';
import Header from './components/header/headerComponent';

function App() {
  return (
    <div className="app">
      <Header />
      <Calculator />
      <Footer />
    </div>
  );
}

export default App;
