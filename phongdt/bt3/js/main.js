function rangeSlide(value) {
    if(value == 0){
        document.getElementById('range1').innerHTML = value ;
    }else{
        document.getElementById('range1').innerHTML = value + ".000.000 đồng";
    }
    console.log(document.getElementById('range1').innerHTML);
};

function rangeMonth(value) {
    document.getElementById('range2').innerHTML = value + " tháng";
    console.log(document.getElementById('range2'))
}
$('input[type=range]').on('input', function(e){
var min = e.target.min,
    max = e.target.max,
    val = e.target.value;

$(e.target).css({
    'backgroundSize': (val - min) * 100 / (max - min) + '% 100%'
});
}).trigger('input');

$("#add-plan").click(function(){
    var count_plan = $(".plan-item").length;
    if(count_plan == 1){
      $("#plan-first").append(`<div class="plan-item">
      <div class="plan-heading">
          <div><b>Phương án 2</b></div>
          <img src="./assets/icons/close.png" alt="img" class="img-title">
      </div>
      <hr class="hr-plan">
      <div class="plan-body">
          <div class="plan-body-item">
              <div>Số tiền vay(Đồng)</div>
              <div class="plan-text-bold">70.000.000</div>
          </div>
          <div class="plan-body-item">
              <div>Thời gian vay(Tháng)</div>
              <div class="plan-text-bold">24</div>
          </div>
          <div class="plan-body-item">
              <div>Lãi suất (%/Tháng)</div>
              <div class="plan-text-bold">1</div>
          </div>
          <div class="plan-body-item">
              <div>Phí trả nợ trước hạn 6 tháng đầu (%)</div>
              <div class="plan-text-bold">1</div>
          </div>
          <div class="plan-body-item ">
              <div>Tổng lãi phải trả(Đồng)</div>
              <div class="plan-text-bold">8.035.616</div>
          </div>
          <div class="plan-body-item ">
              <div>Thu nhập tối thiểu(Đồng)</div>
              <div class="plan-text-bold">8.000.000</div>
          </div>
          <hr class="hr-plan">
          <div class="plan-body-item">
              <div style="line-height: 1.4;">Tiền gốc và lãi phải trả <br>hàng tháng (Đồng)</div>
              <div class="plan-text-bold">4.625.000</div>
          </div>
          <div class="btn-plan">
              <div class="btn-item ">XEM CHI TIẾT <i class="fa fa-angle-right" aria-hidden="true"></i></div>
              <div class="btn-item ">ĐĂNG KÝ NGAY</div>
          </div>
      </div>
     
  </div>`)
    }else if(count_plan == 2){
      $("#plan-first").append(`<div class="plan-item">
      <div class="plan-heading">
          <div><b>Phương án 3</b></div>
          <img src="./assets/icons/close.png" alt="img" class="img-title">
      </div>
      <hr class="hr-plan">
      <div class="plan-body">
          <div class="plan-body-item">
              <div>Số tiền vay(Đồng)</div>
              <div class="plan-text-bold">70.000.000</div>
          </div>
          <div class="plan-body-item">
              <div>Thời gian vay(Tháng)</div>
              <div class="plan-text-bold">24</div>
          </div>
          <div class="plan-body-item">
              <div>Lãi suất (%/Tháng)</div>
              <div class="plan-text-bold">1</div>
          </div>
          <div class="plan-body-item">
              <div>Phí trả nợ trước hạn 6 tháng đầu (%)</div>
              <div class="plan-text-bold">1</div>
          </div>
          <div class="plan-body-item ">
              <div>Tổng lãi phải trả(Đồng)</div>
              <div class="plan-text-bold">8.035.616</div>
          </div>
          <div class="plan-body-item ">
              <div>Thu nhập tối thiểu(Đồng)</div>
              <div class="plan-text-bold">8.000.000</div>
          </div>
          <hr class="hr-plan">
          <div class="plan-body-item">
              <div style="line-height: 1.4;">Tiền gốc và lãi phải trả <br>hàng tháng (Đồng)</div>
              <div class="plan-text-bold">4.625.000</div>
          </div>
          <div class="btn-plan">
              <div class="btn-item ">XEM CHI TIẾT <i class="fa fa-angle-right" aria-hidden="true"></i></div>
              <div class="btn-item ">ĐĂNG KÝ NGAY</div>
          </div>
      </div>
      </div>`);
      $("#add-container").css("display","none");
    }
  })
$("#note-product").click(function(){
      $("#profile").css({"background-color": "#ffffff", "border-bottom-right-radius": "0"});
      $("#note-product").css({"background-color": "#d8d0d0", "border-bottom-left-radius": "10px"});
      var htmlText = `<li class="product-item">Lãi suất trả góp 1% tháng</li>
                      <li class="product-item">Miễn lãi tối đa đến 45 ngày kể từ ngày chi tiêu</li>
                      <li class="product-item">Phí trả nợ trước hạn 6 tháng đầu: 1% số tiền trả nợ trước hạn. Kể từ tháng thứ 7:không thu phí</li>`;
      $("#product-list").html(htmlText);
  })
$("#profile").click(function(){
      $("#profile").css({"background-color": "#d8d0d0", "border-bottom-right-radius": "10px"});
      $("#note-product").css({"background-color": "#ffffff", "border-bottom-left-radius": "0"});
      var htmlText = `<li class="product-item">Cuối mỗi tháng, toàn bộ tiền bạn đã chi tiêu trong tháng sẽ được tổng hợp và chuyển đổi thành 1 khoản trả góp trong 12 tháng.</li>
                      <li class="product-item">Tiền lãi của mỗi khoản trả góp được tính kể từ thời điểm các khoản chi tiêu trong tháng được tổng hợp và chuyển đổi thành khoản trả góp. Lãi phải trả được tính dựa trên dư nợ thực tế tại thời điểm trả lãi (dư nợ gốc giảm dần)</li>
                      <li class="product-item">Tùy thuộc vào nhu cầu chi tiêu hàng tháng, bạn có thể có nhiều hơn 1 khoản trả góp</li>
                      <li class="product-item">Các nội dung và kết quả trên bảng minh họa có thể thay đổi theo từng thời điểm. Ấn chọn 'XEM CHI TIẾT' để xem thêm thông tin</li>`;
      $("#product-list").html(htmlText);
  })
$("#viewPlan").click(function(){
    $("#mobile-plan").css("display","block");
})
$("#add-plan-desktop").click(function(){
    var count_plan = $(".plan-item").length;
    if(count_plan == 1){
      $("#plan-first").append(`<div class="plan-item">
      <div class="plan-heading">
          <div><b>Phương án 2</b></div>
          <img src="./assets/icons/close.png" alt="img" class="img-title">
      </div>
      <hr class="hr-plan">
      <div class="plan-body">
          <div class="plan-body-item">
              <div>Số tiền vay(Đồng)</div>
              <div class="plan-text-bold">70.000.000</div>
          </div>
          <div class="plan-body-item">
              <div>Thời gian vay(Tháng)</div>
              <div class="plan-text-bold">24</div>
          </div>
          <div class="plan-body-item">
              <div>Lãi suất (%/Tháng)</div>
              <div class="plan-text-bold">1</div>
          </div>
          <div class="plan-body-item">
              <div>Phí trả nợ trước hạn 6 tháng đầu (%)</div>
              <div class="plan-text-bold">1</div>
          </div>
          <div class="plan-body-item ">
              <div>Tổng lãi phải trả(Đồng)</div>
              <div class="plan-text-bold">8.035.616</div>
          </div>
          <div class="plan-body-item ">
              <div>Thu nhập tối thiểu(Đồng)</div>
              <div class="plan-text-bold">8.000.000</div>
          </div>
          <hr class="hr-plan">
          <div class="plan-body-item">
              <div style="line-height: 1.4;">Tiền gốc và lãi phải trả <br>hàng tháng (Đồng)</div>
              <div class="plan-text-bold">4.625.000</div>
          </div>
          <div class="btn-plan">
              <div class="btn-item ">XEM CHI TIẾT <i class="fa fa-angle-right" aria-hidden="true"></i></div>
              <div class="btn-item ">ĐĂNG KÝ NGAY</div>
          </div>
      </div>
     
  </div>`)
    }else if(count_plan == 2){
      $("#plan-first").append(`<div class="plan-item">
      <div class="plan-heading">
          <div><b>Phương án 3</b></div>
          <img src="./assets/icons/close.png" alt="img" class="img-title">
      </div>
      <hr class="hr-plan">
      <div class="plan-body">
          <div class="plan-body-item">
              <div>Số tiền vay(Đồng)</div>
              <div class="plan-text-bold">70.000.000</div>
          </div>
          <div class="plan-body-item">
              <div>Thời gian vay(Tháng)</div>
              <div class="plan-text-bold">24</div>
          </div>
          <div class="plan-body-item">
              <div>Lãi suất (%/Tháng)</div>
              <div class="plan-text-bold">1</div>
          </div>
          <div class="plan-body-item">
              <div>Phí trả nợ trước hạn 6 tháng đầu (%)</div>
              <div class="plan-text-bold">1</div>
          </div>
          <div class="plan-body-item ">
              <div>Tổng lãi phải trả(Đồng)</div>
              <div class="plan-text-bold">8.035.616</div>
          </div>
          <div class="plan-body-item ">
              <div>Thu nhập tối thiểu(Đồng)</div>
              <div class="plan-text-bold">8.000.000</div>
          </div>
          <hr class="hr-plan">
          <div class="plan-body-item">
              <div style="line-height: 1.4;">Tiền gốc và lãi phải trả <br>hàng tháng (Đồng)</div>
              <div class="plan-text-bold">4.625.000</div>
          </div>
          <div class="btn-plan">
              <div class="btn-item ">XEM CHI TIẾT <i class="fa fa-angle-right" aria-hidden="true"></i></div>
              <div class="btn-item ">ĐĂNG KÝ NGAY</div>
          </div>
      </div>
      </div>`);
      $("#add-container-desktop").css("display","none");
    }else{
        alert("Bạn chỉ được tạo 3 phương án")
    }
  })