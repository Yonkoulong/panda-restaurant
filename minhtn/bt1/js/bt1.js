var old_slide = 6;
var current_slide = 1;

function changeStyle(x) {
    return document.getElementById(x).style;
}

function showSlide() {
    changeStyle('slide_'+current_slide).display = 'block';
    changeStyle('dot_'+current_slide).background = 'black';
    changeStyle('slide_'+old_slide).display = 'none';
    changeStyle('dot_'+old_slide).background = 'gray';
}   

function leftSlide() {
    old_slide = current_slide;
    if (current_slide>1) current_slide--;
    else current_slide = 6;
    showSlide();
}

function rightSlide() {
    old_slide = current_slide;
    if (current_slide<6) current_slide++;
    else current_slide = 1;
    showSlide();
}

setInterval(rightSlide,5000);

var a = 1;
function nav(){
    if( a == 1){
        document.getElementById("nav").style.display = 'none';
    }else if( a == -1){
        document.getElementById("nav").style.display = 'block';
    }
    a *= -1;
}