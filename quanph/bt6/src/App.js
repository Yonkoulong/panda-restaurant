import logo from './logo.svg';
import './App.css';
import react from 'react'
import Navbar from './pages/Navbar'
import Products from './pages/products'
import Cart from './pages/Cart'
import CartIcons from './components/cartIcons'
import product from './pages/product';
import store from './store/store'
import { Provider } from 'react-redux';
// import Home from './components/Home'
import {BrowserRouter as Router, Route , Link} from 'react-router-dom'
function App() {

  return (
    <Router>
    <div className="App">
       <header>
        <nav className='container header'>
          <h2>Shop</h2>
          <ul className='navbar'>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/products">Products</Link></li>
          <li className='cart'><Link to="/cart" className='cart-item'>Cart</Link>
          <CartIcons/>
          </li>
          </ul>
        </nav>

      </header>
 
    <Route exact path='/' component={Navbar}/>
    <Route exact path='/products' component={Products}/>
    <Route path='/products/:id' component={product}/>
    <Route path='/cart' component={Cart}/>
    {/* <Home/> */}
    </div>
    </Router>
  );
}
function AppWithStore(){
  return <Provider store={store}>
    <App/>
  </Provider>
}

export default AppWithStore;
