import Products from '../api/db.json'

export function getAll(){
    return Promise.resolve(Products);
}

export function getById(id){
    const product = Products.find(items => items.id === id)
    return Promise.resolve(product);

}

export default {
    getAll,
    getById
}