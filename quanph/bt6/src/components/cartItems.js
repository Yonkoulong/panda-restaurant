import React from 'react';
import {connect} from "react-redux"
import {removeFromCart} from "../store/actions/actions"

 function CartItems(props) {
    const {item, index} = props;
    const {product} = item;
    return(
        <div >
            <img src={product.img}/>
            <h3>{product.name}</h3>
            <p className="price">
            Price:{product.price} 
            <br/>Quantity: {item.quantity}
             <br/> Total: {item.quantity * product.price}</p>
            <button onClick={()=> props.removeFromCart(index)} className="addToCart">Delete</button>
        </div>
    )
}
const mapDispatchToProps = (dispatch) =>{
    return {
       removeFromCart: (index) =>dispatch(removeFromCart(index)),
    }
  }

export default connect(null, mapDispatchToProps)(CartItems);
