import React from 'react';
import {Link} from 'react-router-dom'

export default function ProductItems(props) {
    const {product} = props
    return(
        <div >
            <img src={product.img}/>
            <h3>{product.name}</h3>
            <p className="price">{product.price}</p>
            <Link to={"/products/" + product.id} className="addToCart">Details</Link>
        </div>
    )
}