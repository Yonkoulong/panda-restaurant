import React, {useState} from 'react';
import axios from 'axios';
import NewHome from '../NewHome';
import {useSelector, useDispatch} from 'react-redux';
import { addNewProducts } from '../actions/actions';
// import { getlist } from '../reducers/reducer';


function Home (){
const url = "http://localhost:3100/api/sanpham"
 

const  formData = useSelector(state => state.cart.list);


const dispatch = useDispatch()

const handleClick = async ()=>{
  const res = await axios.get(url)
  const newList = res.data
  const action = addNewProducts(newList)
  dispatch(action)
}

return(
  
  <div className="container">
    <NewHome
      button="Add to cart"
    />
     <button className='addToCart' onClick={handleClick}>Add to Buy</button>
   {formData.map(item => {
                return (
                    <div className="element" key={item.id}>
                        <img src={item.img}/>
                        <h3>{item.name}</h3>
                        <p>{item.price}</p>
                    </div>
                )
            })}
  </div>
)
}
export default Home;