import React from "react";
import ProductItems from "../components/ProductItems"
import ProductsApi from "../api/product"

export default class Product extends React.Component {

  state = {
    products: [],
  }

  componentDidMount(){
    ProductsApi.getAll()
      .then(data =>{
        console.log(data);
        this.setState({
          products: data
        })
      })
  }

  render() {
    return (
      <>
      <h1 className='heading'>Products</h1>
      <div className="products">
        {this.state.products.map(product =>
        <div className="element" key={product.id}>
          <ProductItems product={product}/>
        </div>
          )}
      </div>
      </>
  
    );
  }
}


