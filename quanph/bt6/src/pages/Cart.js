import React from "react";
import CartItems from "../components/cartItems"
import ProductsApi from "../api/product"
import {connect} from "react-redux"
import {clearCart} from "../store/actions/actions"


 class Cart extends React.Component {

  placeOrder=() => {
    this.props.clearCart()
    alert('We receive your order, and we are working on it!')
  }

  render() {
    return (
      <>
      <h1 className='heading'>Cart</h1>
      <div className="container">
        {this.props.cartItems.map((item, index) =>
        <div className="element" key={item.product.id}>
          <CartItems item={item} index={index}/>
        </div>
          )}
          <h3>Total: {this.props.total}</h3>
          <br></br>
          <button onClick={this.placeOrder}>Place Order</button>
      </div>
      </>
  
    );
  }
}

const mapStateToProps = (state) =>{
  return {
      cartItems : state.cart,
      total: state.cart.reduce((total, item) => total + item.quantity * item.product.price, 0)
  }
}


const mapDispatchToProps = (dispatch) =>{
  return {
     clearCart: () =>dispatch(clearCart()),
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Cart);
