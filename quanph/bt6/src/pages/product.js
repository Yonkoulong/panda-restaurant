import React from "react";
import {getById} from "../api/product"
import {addToCart} from "../store/actions/actions";
import {connect} from "react-redux"


 class product extends React.Component {

    state = {
        product: {},
        quantity:0
    }

    componentDidMount(){
        const id= this.props.match.params.id;
        console.log(id);

        getById(parseInt(id))
        .then(product => {
            console.log({product});
            this.setState({
                product
            });
        })
    }
    handleQuantity=(event) => {
        const value = event.target.value
        if(value < 0)
            return;
        
        this.setState({
            quantity:value
        })
    }
    addToCart=(product)=>{
        this.props.addToCart(product, this.state.quantity)
    }

    render() {
        const product = this.state.product
        const quantity = this.state.quantity
        return (
          <div>
              <div className='container product-detail'>
              <img src={product.img}/>
            <div className='product-item-detail'>
              <h1 className='heading'>{product.name}</h1> 
              <p> Price:{product.price} đ </p>
              <p>{product.detail}</p>
              <input type="number"  value={quantity} onChange={this.handleQuantity}/>
              <br></br>
              <p>Total: {quantity * product.price}</p>
              <button className='addToCart' onClick={()=>this.addToCart(product)}>Add To Cart</button>
            </div>
              </div>
          </div>
        );
    }
}

const mapDispatchToProps = (dispatch) =>{
    return {
        addToCart: (productsInfo, quantity) =>dispatch(addToCart(productsInfo, quantity)),
    }
  }

export default connect(null,mapDispatchToProps)(product)