import { createStore, compose, applyMiddleware,} from 'redux';
import logger from 'redux-logger';
import cartReducer from './reducers/index'

const initialState = {
    cart: []
};





const store = createStore(cartReducer, initialState, compose( 
    applyMiddleware(logger),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    ));
 

export default store
