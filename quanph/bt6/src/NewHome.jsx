import React from 'react';
import axios from 'axios';
import Home from './components/Home'
  class NewHome extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listData:[]
        }
    }

    async componentDidMount(){
        let result = await axios.get('http://localhost:3100/api/sanpham');
        this.setState({
            listData:result.data && result ? result.data : []
            
         })
         console.log(result.data);
    }
    render(){
        return (
            <>
               {this.state.listData.map(item => {
                return (
                    <div className="element" key={item.id}>
                        <img src={item.img}/>
                        <h3>{item.name}</h3>
                        <p>{item.price} đ</p>
                        <button className='addToCart'>{props.button}</button>
                    </div>
                )
            })}
            </>
        )
    }
  }

  export default NewHome