$.fn.jqueryCarousel = function (options) {

  var defaults = {
    id: "",
    autoScroll: false,
    infinite: true,
    scrollRight: true,
    dot: true,
    slideToShow: 1,
    SlideToScroll: 1,
    resHeight: true
  };

  options = $.extend(defaults, options); //parameters

  const actions = (function () {
    const id = options.id;
    const sliderList = document.querySelector(`${id} .slider-list`);
    const allSlider = document.querySelectorAll(`${id} .slider-img`);
    const slideLength = allSlider.length;
    const slidersWidth = $('.slider-list img').width();
  
    let index = 0;
    let posX1;
    let posX2;
    let initialPositon;
    let finalPositon;
    let canISlide = true;


    return {
      init(ell) {
        this.getViewport(ell);
      },
      getViewport(ell) {
        const wrapper = $("<div/>", {
          class: "slider"
        });
        const viewport = $("<div/>", {
          class: "slider-items"
        });
        $(viewport)
          .append(this.addIndicator(ell))
          .append(this.addBtns(ell))
          .append(this.addItems(ell))
        $(wrapper).append(viewport);
        $(ell).append(wrapper);
        this.cloneSLide(ell);
        this.responsiveHeight(ell);
        this.Indicatorwork(ell);
      },

      getIndicator(ell) {
        const indicator = ell.find(".dot-list .dot");
        return indicator;
      },

      addBtns(ell) {
        const btnDiv = $("<div/>", {
          class: "btn-container"
        });
        const next = $("<div/>", {
          class: "button prevBtn",
          text: ">"
        });
        next.on("click", () => this.BtnSwitchSlide("next"));
        const prev = $("<div/>", {
          class: "button nextBtn",
          text: "<"
        });
        prev.on("click", () => this.BtnSwitchSlide("prev"));
        return btnDiv.append(next, prev)
      },

      addItems(ell) {
        const itemContainer = $("<div/>", {
          class: "item-container"
        });
        $(itemContainer).append($(ell).find('ul'))
        return itemContainer
      },

      addIndicator(ell) {
        const indicatorContainer = $("<div/>", {
          class: "indicator-container"
        });
        const ul = $('<ul>', {
          class: "dot-list"
        });

        for (let i = 0; i < $(ell).find('li').length; i++) {
          const li = $(`<li class="dot ${i == 0 ? "active" : ""}" data-index="${i}"  ></li>`);
          $(ul).append(li);
        }
        return indicatorContainer.append(ul)
      },



      Indicatorwork(ell) {
        const $this = this;
        const len = ell.find(".item-container .slider-img").length - 2;
        const indicator = ell.find(".dot-list .dot");
        for (let i = 0; i <= len; i++) {
          sliderList.classList.add("transition");
          $(indicator[i]).on("click", () => {
            index = i;
            if (index == 0) {
              index = i;
              sliderList.style.left = `${-slidersWidth}px`;


            } else {
              index = i;
              $(indicator[i]).addClass("active");
              sliderList.style.left = `${-slidersWidth * (index + 1)}px`;

            }
            $this.responsiveHeight();
            $(`${id} .dot`).removeClass("active");
            $(indicator[i]).addClass("active");

          })
        }
      },

      adddListener(ell) {
        this.swipeSlide(ell);
      },


      // swipe,drag slide
      swipeSlide(ell) {

        sliderList.addEventListener("mousedown", dragStart);
        sliderList.addEventListener("touchstart", dragStart);
        sliderList.addEventListener("touchmove", dragMove);
        sliderList.addEventListener("touchend", dragEnd);
        const $this = this;

        function dragStart(e) {
          e.preventDefault();
          initialPositon = sliderList.offsetLeft;
          if (e.type == "touchstart") {
            posX1 = e.touches[0].clientX;
          } else {
            posX1 = e.clientX;

            document.onmouseup = dragEnd;
            document.onmousemove = dragMove;
          }
        }

        function dragMove(e) {
          if (e.type == "touchmove") {
            posX2 = posX1 - e.touches[0].clientX;
            posX1 = e.touches[0].clientX;
          } else {
            posX2 = posX1 - e.clientX;
            posX1 = e.clientX;
          }
          sliderList.style.left = `${sliderList.offsetLeft - posX2}px`;
        }

        function dragEnd() {
          finalPositon = sliderList.offsetLeft;
          if (finalPositon - initialPositon < -496) {
            $this.BtnSwitchSlide("next", "dragging");
          } else if (finalPositon - initialPositon > 400) {
            $this.BtnSwitchSlide("prev", "dragging");
          } else {
            sliderList.style.left = `${initialPositon}px`;
          }
          document.onmouseup = null;
          document.onmousemove = null;
        }

      },


      //  tạo clone đầu cuối
      cloneSLide(ell) {
        const firstSlide = allSlider[0];
        const lastSlide = allSlider[allSlider.length - 1];

        const cloneFirstSlide = firstSlide.cloneNode(true);
        const cloneLastSlide = lastSlide.cloneNode(true);

        sliderList.appendChild(cloneFirstSlide);
        sliderList.insertBefore(cloneLastSlide, firstSlide);
        sliderList.addEventListener("transitionend", this.checkIndex);
      },

      BtnSwitchSlide(arg, arg2) {
        const $$this = this;
        sliderList.classList.add("transition");
        if (canISlide) {
          if (!arg2) {
            initialPositon = sliderList.offsetLeft;
          }
          if (arg == "next") {
            sliderList.style.left = `${initialPositon - slidersWidth}px`;
            index++;
          } else {
            sliderList.style.left = `${initialPositon + slidersWidth}px`;
            index--;
          }
        }
        canISlide = false;

        $(`${id} .dot`).removeClass("active");
        const indicator = document.querySelectorAll(`${id} .dot`);
        $(indicator[index]).addClass("active");
        $$this.responsiveHeight();


      },
      // khi clone
      checkIndex() {
        sliderList.classList.remove("transition");
        if (index == -1) {
          sliderList.style.left = `-${slideLength * slidersWidth}px`;
          index = slideLength - 1;
        }
        if (index == slideLength) {
          sliderList.style.left = `-${1 * slidersWidth}px`;
          index = 0;
        }

        canISlide = true;

        $(`${id} .dot`).removeClass("active");
        const indicator = document.querySelectorAll(`${id} .dot`);
        $(indicator[index]).addClass("active");
      },

      responsiveHeight(ell) {
        const sliderId = $(id);
        const SLider = sliderId.find(".slider-items");
        const imgSlide = $(`${id} .slider-list .slider-img img`);
        for (let i = 0; i < imgSlide.length; i++) {
          if (index == i - 1) {
            let imgHeight = $(imgSlide[i]).height();
            console.log(imgHeight + "==" + i);
            $(imgSlide[i]).css({
              "height": "auto"
            });
            SLider.css({
              height: imgHeight + "px",
            });
          }
        }
      },

    }
  })()

  return this.each(function () {
    const ell = $(this);
    actions.init(ell)
    actions.adddListener(ell);
  });

};

$(".container").jqueryCarousel({
  autoScroll: false,
  scrollRight: true,
  id: "#sliders-1",
  infinite: true,
  dot: true,
  slideToShow: 1,
  SlideToScroll: 1,
  resHeight: true
});

$(".Carousel").jqueryCarousel({
  autoScroll: false,
  scrollRight: true,
  infinite: true,
  id: "#sliders-2",
  dot: true,
  slideToShow: 1,
  SlideToScroll: 1
});