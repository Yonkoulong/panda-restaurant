import React from "react";
import './calc.css';

class Calculator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Clear:"",
            number1:"",
            number2:"",
            calc:"",  
            result:"",
            callback:[],
            lunisolar:'' ,
        }
    }


    handleNumber =(event)=>{
          var One = this.state.number1;
          var Two = this.state.number2;
          One += event.target.value;
        if(this.state.calc.length > 0){
            Two += event.target.value
            this.setState({
                number2: Two,
            })
        }
        else{
            this.setState({
                number1: One,
                
            })
        }
        

    }

    Lunisolar=(event)=>{
        var LUNISOLAR= this.state.lunisolar;
        LUNISOLAR += event.target.value;
        if( LUNISOLAR === '-'){
            this.setState({
                lunisolar:'-',
               LUNISOLAR: LUNISOLAR * 0,
            })
        }
        else{
            this.setState({
                lunisolar:""
            })
        }
    }
   
    handleCalc=(event)=>{
        this.setState({
            calc: event.target.value,
        })
    }
    ClearAll=(event)=>{
        this.setState({
            number1: '',
            number2: '',
            calc:'', 
            Clear: '',
            result: '',
            lunisolar: '',
        })
    }
    Result=(event)=>{
        var plus =parseInt(this.state.number1) + parseInt(this.state.number2);
        var multiple = this.state.number1 * this.state.number2;
        var minus = this.state.number1 - this.state.number2;
        var division = this.state.number1 / this.state.number2;
        var percent = this.state.number1 / 100 * this.state.number2;
        var Exponent2 = this.state.number1 * this.state.number1;
        
        var result1 = this.state.number1 * 1000000000
        var result2 = this.state.number2 * 1000000000
       

        if(this.state.calc === '/'){
           
            this.setState({
                result :division,
            })
            this.setState({
                callback:[
                    ...this.state.callback,
                    {
                    number1:this.state.number1, number2:this.state.number2, calc:'/', result:division,
                }]
            })
        if(this.state.lunisolar ==='-'){
                this.setState({
                    result: -1 * division,
                    callback:[
                        {number1:this.state.number1, number2:this.state.number2, calc:'/',lunisolar:this.state.lunisolar,result: -1 * division}
                    ]
                })
        }
        }
        else if (this.state.calc === '-'){
            var resultFinal = (result1 - result2 )/ 1000000000
            if (this.state.lunisolar ==='-'){
                this.state.number1 = -this.state.number1
                var resultFinal = (-result1 - result2 )/ 1000000000

            }

            this.setState({
                result :resultFinal,
                lunisolar: ''
            })

            this.setState({
                callback:[
                    ...this.state.callback,
                    {number1:this.state.number1, number2:this.state.number2, calc:'-', result: resultFinal,
                }]
            })
        }
        else if (this.state.calc === 'x'){
      
                var resultFinal = result1 * result2 /1000000 /1000000 / 1000000
                
            this.setState({
                result : resultFinal,    
            
            })
            this.setState({
                callback:[
                    ...this.state.callback,
                    {number1:this.state.number1, number2:this.state.number2, calc:'x', result:resultFinal
                }]
            })
            if(this.state.lunisolar ==='-'){
                this.setState({
                    result: -1 * resultFinal,
                    callback:[
                        {number1:-this.state.number1, number2:this.state.number2, calc:'x', result: -resultFinal,
                    }]
                })
            }
        }
        else if (this.state.calc === '+'){
            var resultFinal = (result1 + result2 )/ 1000000000
            if (this.state.lunisolar ==='-'){
                this.state.number1 = -this.state.number1
                var resultFinal = (-result1 + result2 )/ 1000000000
            }
            this.setState({
                result :resultFinal,
                lunisolar:''
            })
            this.setState({
                callback:[
                    ...this.state.callback,
                    {
                    number1:this.state.number1, number2:this.state.number2, calc:'+', result:resultFinal,
                }]
            })
        }
        else if (this.state.calc === '%'){
            this.setState({
                result :percent,
            })
            this.setState({
                callback:[  
                    {
                        number1:this.state.number1, number2:this.state.number2, calc:'%', result:percent
                    }]
            })
            if(this.state.lunisolar ==='-'){
                    this.setState({
                        result: -1 * percent,
                        callback:[
                            {number1:this.state.number1, number2:this.state.number2, calc:'%', result:-percent,lunisolar:this.state.lunisolar}
                        ]
                    })
            }
        }
        else if (this.state.calc === '^2'){
            var resultFinal = (result1 * result1 )/ 1000000000 / 1000000000

            this.setState({
                result:resultFinal,
            })
            this.setState({
                callback:[
                    ...this.state.callback,
                    {number1:this.state.number1, calc:'^2', result: Exponent2,lunisolar:this.state.resultFinal
                }]
            })
        }

    }


    Reload=((ind)=>{
     
        this.setState({
            callback: this.state.callback.filter((item,index)=>
                index !== ind
            ),
            number1: this.state.callback[ind].number1,
            number2: this.state.callback[ind].number2, 
            calc: this.state.callback[ind].calc,
            lunisolar: this.state.callback[ind].lunisolar,
            result: this.state.callback[ind].result,

        })
    })
    render() { 

    return(
        <div className="Container">
  
        <p  className="calc-num">
        {this.state.lunisolar}{this.state.number1}{this.state.calc}{this.state.number2}
        </p>
        <p className="calc-num">
            {this.state.result}
        </p>
        <div className="keypad">
            <button id="clear" onClick={this.ClearAll}>Clear</button>
            <button id="backspace" disabled={this.state.callback.length <=0} onClick={()=>{this.Reload(this.state.callback.length -1)}}>C</button>
            <button value='7' onClick={this.handleNumber}>7</button>
            <button value='8' onClick={this.handleNumber}>8</button>
            <button value='9' onClick={this.handleNumber}>9</button>
            <button value='/'disabled={!this.state.number1}  onClick={this.handleCalc} >/</button>
            <button value='4' onClick={this.handleNumber}>4</button>
            <button value='5' onClick={this.handleNumber}>5</button>
            <button value='6' onClick={this.handleNumber}>6</button>
            <button value='^2'disabled={!this.state.number1}  onClick={this.handleCalc} >^2</button>
            <button value='1' onClick={this.handleNumber}>1</button>
            <button value='2' onClick={this.handleNumber}>2</button>
            <button value='3' onClick={this.handleNumber}>3</button>
            <button value='x' disabled={!this.state.number1} onClick={this.handleCalc}>x</button>
            <button value='0' onClick={this.handleNumber}>0</button>
            <button value='.' onClick={this.handleNumber}>.</button>
            <button value='-'  onClick={this.Lunisolar} >+-</button>    
            <button value='-' disabled={!this.state.number1} onClick={this.handleCalc}>-</button>
            <button value='=' className='grid-col' disabled={!this.state.number1} onClick={this.Result}>=</button>
            <button value='+' disabled={!this.state.number1} onClick={this.handleCalc}>+</button>
            <button value='%' disabled={!this.state.number1} onClick={this.handleCalc}>%</button>
        </div>
  
        </div>
    )}  
}   

export default Calculator;