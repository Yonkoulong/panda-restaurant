const slider = document.querySelector('.slider');
const slide = document.querySelectorAll('.slide');
const slidess = document.querySelector('#slider');
const container = document.querySelector('.carousel');
const dotss = document.querySelectorAll('.dot');
console.log(dotss);
let size = 1;
const slideWidth = slide[0].clientWidth;
const slideLength = slide.length;
console.log(slideWidth);
const left = slider.style.transform = 'translateX(' + (-slideWidth * size) + 'px)';

// Create next button
let nextBtnCre = document.createElement('button');
nextBtnCre.innerHTML = '<img src="img_show/icon-controller-right.png" >';
nextBtnCre.className = 'right';
nextBtnCre.onclick = function () {
  nextBtn();
  sliderFor()
}
slidess.appendChild(nextBtnCre);
// Create previous button
let prevBtnCre = document.createElement('button');
prevBtnCre.innerHTML = '<img src="img_show/icon-controller-left.png" >';
prevBtnCre.className = 'left';
prevBtnCre.onclick = function () {
  prevBtn();
  sliderFor()
}
slidess.appendChild(prevBtnCre);

// Create slider forEach
function sliderFor() {
  slider.addEventListener('transitionend', () => {
    if (slide[size].id === 'lastSlide') {
      slider.style.transition = "none";
      size = slide.length - 2;
      slider.style.transform = 'translateX(' + (-slideWidth * size) + 'px)';
    };
    if (slide[size].id === 'firstSlide') {
      slider.style.transition = "none";
      size = slideLength - size;
      slider.style.transform = 'translateX(' + (-slideWidth * size) + 'px)';
    };
  });
};

function prevBtn() {
  if (size <= 0) return;
  slider.style.transition = "transform 0.4s ease-in-out";
  size--;
  slider.style.transform = 'translateX(' + (-slideWidth * size) + 'px)';
  const dotss = document.querySelectorAll('.dot');
  [...dotss].forEach((el) => el.classList.remove("active"));
  dotss[size].classList.add("active");
  console.log(-slideWidth * size);
};

function nextBtn() {
  if (size >= slideLength - 1) return;
  let sizedot = size;
  if (sizedot >= slideLength - 2) sizedot = 0;
  slider.style.transition = "transform 0.4s ease-in-out";
  size++;
  slider.style.transform = 'translateX(' + (-slideWidth * size) + 'px)';
  const dotss = document.querySelectorAll('.dot');
  [...dotss].forEach((el) => el.classList.remove("active"));
  dotss[sizedot].classList.add("active");
  console.log(size);
};



// Create dot slide
let dots = document.createElement('div');
dots.className = 'dots';
dots.innerHTML = `<div  class="dot active" data-index="1"></div>
                   <div  class="dot" data-index="2"></div>
                   <div  class="dot" data-index="3"></div>
                   <div  class="dot" data-index="4"></div>`
dots.onclick = function (e) {
  const dotss = document.querySelectorAll('.dot');
  [...dotss].forEach((el) => el.classList.remove("active"));
  e.target.classList.add("active");
  const slideindex = parseInt(e.target.dataset.index);
  size = slideindex;
  slider.style.transform = 'translateX(' + (-slideWidth * size) + 'px)';
}
document.body.appendChild(dots);


//Create touch slide
let isDragging = false,
  startPos = 0,
  currentTranslate = 0,
  prevTranslate = 0,
  animationID,
  currentIndex = 1;

const slides = Array.from(document.querySelectorAll('.slide1'));

// add our event listeners
slides.forEach((slide, index) => {
  const slideImage = slide.querySelector('img');
  // disable default image drag
  slideImage.addEventListener('dragstart', (e) => e.preventDefault());
  // touch events
  slide.addEventListener('touchstart', touchStart(index));
  slide.addEventListener('touchend', touchEnd);
  slide.addEventListener('touchmove', touchMove);
  // mouse events
  slide.addEventListener('mousedown', touchStart(index));
  slide.addEventListener('mouseup', touchEnd);
  slide.addEventListener('mousemove', touchMove);
  slide.addEventListener('mouseleave', touchEnd);
})

// make responsive to viewport changes
window.addEventListener('resize', setPositionByIndex)

// prevent menu popup on long press
window.oncontextmenu = function (event) {
  event.preventDefault()
  event.stopPropagation()
  return false
}

function getPositionX(event) {
  return event.type.includes('mouse') ? event.pageX : event.touches[0].clientX
}

// use a HOF so we have index in a closure
function touchStart(index) {
  return function (event) {
    currentIndex = index
    startPos = getPositionX(event)
    isDragging = true
    animationID = requestAnimationFrame(animation)
    slider.classList.add('grabbing')
  }
}

function touchMove(event) {
  if (isDragging) {
    const currentPosition = getPositionX(event);
    currentTranslate = prevTranslate + currentPosition - startPos;
  }
}

function touchEnd() {
  cancelAnimationFrame(animationID)
  isDragging = false
  const movedBy = currentTranslate - prevTranslate
  // if moved enough negative then snap to next slide if there is one
  if (movedBy < -100 && currentIndex < slideLength - 1) size ++;

  // if moved enough positive then snap to previous slide if there is one
  if (movedBy > 100 && currentIndex > 0) size --;
  // sliderFor();
  setPositionByIndex()
  slider.classList.remove('grabbing')
}

function animation() {
  setSliderPosition()
  if (isDragging) requestAnimationFrame(animation)
}

function setPositionByIndex() {
  currentTranslate = size * -slideWidth;
  prevTranslate = currentTranslate;
  setSliderPosition();
}

function setSliderPosition() {
  slider.style.transform = `translateX(${currentTranslate}px)`;
  slider.style.transition = "transform 0.4s ease-in-out";
  let sizedot = size;
  if (sizedot >= slideLength - 2) sizedot = 0;
  const dotss = document.querySelectorAll('.dot');
  [...dotss].forEach((el) => el.classList.remove("active"));
  dotss[sizedot].classList.add("active");
  sliderFor();
}

// window.addEventListener('resize', ()=>{
//   var widthd = window.innerWidth;
//   const slidecss = document.querySelector('.slide');
//   console.log(widthd);
//   console.log(slidecss);
//   if(widthd >= 1200){
//     slidecss.style.width = '1200px';
//     container.style.width = '1200px';
//   }else
//   if( widthd >= 768 && widthd < 1200){
//     slidecss.style.maxWidth = '100%';
//     slidecss.style.width = '100%';
//     container.style.width = '100%';
//     container.style.maxWidth = '100%';
//   }else if( widthd < 786){
//     slidecss.style.width = '100%';
//     container.style.width = '100%';
//     slidecss.style.maxWidth = '100%';
//     container.style.maxWidth = '100%';
//   }
  
// });