$.fn.jqueryCarousel = function (options) {

    var defaults = {
        id: "",
        autoScroll: false,
        infinite: true,
        scrollRight: true,
        dot: true,
        slideToShow: 1,
        SlideToScroll: 1,
        resHeight: true
    };

    options = $.extend(defaults, options); //parameters

    const actions = (function () {
        let counter = 0;
        let isDragging = false,
            startPos = 0,
            currentTranslate = 0
        return {
            init(el) {
                this.getViewport(el)
            },
            getViewport(el) {
                const wrapper = $("<div/>", { class: "carousel-wrapper" });
                const viewport = $("<div/>", { class: "carousel-viewport" });

                //   debugger
                $(viewport)
                    .append(this.addIndicator(el))
                    .append(this.addBtns(el))
                    .append(this.addItems(el))
                $(wrapper).append(viewport)
                $(el).append(wrapper)
                this.cloneItem(el);
                this.setStyleIndicator(el);
            },
            addBtns(el) {
                const btnDiv = $("<div/>", { class: "btn-container" });
                const btnPrevDiv = $("<button/>", { class: "button prevBtn", text: "" });
                const btnNextDiv = $("<button/>", { class: "button nextBtn", text: "" });
                const imgNext = $("<img/>", { src: "img_show/icon-controller-right.png" });
                const imgPrev = $("<img/>", { src: "img_show/icon-controller-left.png" });
                btnDiv.append(btnPrevDiv)
                btnDiv.append(btnNextDiv)
                btnNextDiv.append(imgNext)
                btnPrevDiv.append(imgPrev);
                return btnDiv
            },
            addItems(el) {
                const itemContainer = $("<div/>", { class: "item-container" });
                $(el).find('ul li').addClass("slide-li");
                $(itemContainer).append($(el).find('ul').addClass("slider-main"))
                return itemContainer
            },
            addIndicator(el) {
                const indicatorContainer = $("<div/>", { class: "indicator-container" });
                const ul = $('<ul>', { class: "dot-main" });
                for (let i = 0; i < $(el).find('li').length; i++) {
                    const li = $('<li>', { class: "dot", dataIndex: i })
                    $(ul).append(li);
                }
                return indicatorContainer.append(ul)
            },
            setStyleIndicator(el) {
                const indBar = el.find(".dot-main");
                const len = el.find(".item-container .slide-li").length - 2;
                for (let i = 0; i <= len; i++) {
                    indBar.css("width", 40 * len + "px");
                }
            },

            cloneItem(el) {
                firstItem = el.find(".item-container .slide-li").filter(':first-child'),
                    lastItem = el.find(".item-container .slide-li").filter(':last-child'),
                    firstItem.before(lastItem.clone(true));
                lastItem.after(firstItem.clone(true));
            },


            adddListener(el) {
                this.addEventListener(el)
                this.addIndicatorEvent(el)
                this.addDragEvent(el)
            },

            transitionSlide(el, isTransition) {
                let size = el.find(".carousel-viewport").width();
                el.find(".item-container .slider-main").css("transition", isTransition ? "transform 0.4s ease-in-out" : "none");
                el.find(".item-container .slider-main").css("transform", 'translateX(' + (-size * counter) + 'px)');

            },

            addEventListener(el) {
                const $this = this;
                let size = el.find(".carousel-viewport").width();
                const len = el.find(".item-container .slide-li").length - 2;
                const prevBtn = el.find(".btn-container .prevBtn");
                const nextBtn = el.find(".btn-container .nextBtn");
                const indicator = el.find(".dot-main .dot");
                // el.find(".item-container .slider-main").css("transform", 'translateX(' + (-size) + 'px)');
                function triggers(changeCounter) {
                    counter = changeCounter ? counter -= options.SlideToScroll : counter += options.SlideToScroll;
                    $this.transitionSlide(el, true);
                    for (let i = 0; i <= indicator.length; i++) {
                        $(indicator[i]).css("backgroundColor", (i == counter) ? "white" : "black");
                    }
                    $this.responsiveHeight(el);
                }
                prevBtn.on("click", () => {
                    if (counter <= -1) return $(indicator[0]).css("backgroundColor", "white");
                    triggers(true);
                })
                nextBtn.on("click", () => {
                    if (counter >= len) return $(indicator[0]).css("backgroundColor", "white");
                    triggers(false);
                })
                // event 
                el.find('.item-container .slider-main').on("transitionend", () => {
                    for (let i = 0; i <= indicator.length; i++) {
                        counter < 0 ? counter = len - 1 :
                            0 <= counter && counter <= len - 1 ? this.transitionSlide(el, false) :
                                counter = 0;
                        $(indicator[i]).css("backgroundColor", (i == counter) ? "white" : "black");
                    }
                })
            },
            windowLoad(el) {
                const $this = this;
                $(window).on("load", () => {
                    $this.responsiveHeight(el);
                    let size = el.find(".carousel-viewport").width();
                    $this.transitionSlide(el, false);
                    el.find(".slide-ul").css({
                        "left": "-" + size + "px"
                    })
                })
            },

            responsiveHeight(el) {
                const imgSlide = el.find(`.item-container .slider-main .slide-li img`);
                for (let i = 0; i < imgSlide.length; i++) {
                    if (counter == i) {
                        let imgHeight = $(imgSlide[i]).height();
                        $(imgSlide[i]).css({
                            "height": options.resHeight ? "auto" : "900px"
                        });
                        el.find(".carousel-viewport").css({
                            "height": options.resHeight ? imgHeight : "900" + "px"
                        })
                        el.find(".carousel-viewport .item-container").css({
                            "height": options.resHeight ? imgHeight : "900" + "px"
                        })
                    }
                }
            },
            addIndicatorEvent(el) {
                const $this = this;
                const len = el.find(".item-container .slide-li").length - 2;
                const indicator = el.find(".dot-main li");
                for (let i = 0; i <= len; i++) {
                    // indicator click and event
                    $(indicator[i]).on("click", () => {
                        counter = i;
                        // $(indicator[i]).css("background", (i == counter) ? 'white' : 'black')
                        $this.transitionSlide(el, true);
                        $this.responsiveHeight(el);
                    })
                }
            },
            addDragEvent(el) {
                const $this = this;
                const carouselItems = el.find(".slider-main .slide-li")
                Array.from(carouselItems).forEach((slide) => {
                    $(slide).find("img").on('dragstart', (e) => e.preventDefault());
                    $(slide).on('touchstart', $this.onTouchStart(el));
                    $(slide).on('touchmove', $this.onTouchMove(el));
                    $(slide).on('touchend', $this.onTouchEnd(el));
                    $(slide).on('mousedown', $this.onTouchStart(el));
                    $(slide).on('mousemove', $this.onTouchMove(el));
                    $(slide).on('mouseup', $this.onTouchEnd(el));
                    $(slide).on('mouseleave', $this.onTouchEnd(el));
                })
            },
            getPositionX(event) {
                return event.type.includes('mouse') ? event.pageX : event.originalEvent.touches[0].pageX;
            },
            onTouchStart(el) {
                const $this = this;
                return function (event) {
                    startPos = $this.getPositionX(event);
                    isDragging = true;
                    el.find(".carousel-viewport").css("cursor", "grabbing");
                }
            },
            onTouchMove(el) {
                const $this = this;
                return function (event) {
                    if (isDragging) {
                        const currentPosition = $this.getPositionX(event);
                        currentTranslate = currentPosition - startPos;
                    };
                }
            },
            onTouchEnd(el) {
                const $this = this;
                const len = el.find(".item-container .slide-li").length - 2;
                return function (event) {
                    isDragging = false;
                    if (currentTranslate > 100) {
                        counter = counter > 0 ? counter -= options.SlideToScroll :
                            counter == 0 && options.infinite ? counter -= options.SlideToScroll : counter;
                        console.log("ad");
                    } else if (currentTranslate < -100) {
                        counter = counter < len - 1 ? counter += options.SlideToScroll :
                            counter == len - 1 && options.infinite ? counter += options.SlideToScroll : counter;
                    } else {
                        counter = counter;
                    }
                    $this.transitionSlide(el, true);
                    $this.responsiveHeight(el);
                    el.find(".carousel-viewport").css("cursor", "grab")
                    currentTranslate = 0;
                }
            }
        }
    })()

    return this.each(function () {
        const el = $(this);
        actions.init(el)
        actions.adddListener(el);
    });

};

$(".carousel").jqueryCarousel({
    autoScroll: false,
    scrollRight: true,
    id: "#sliders-1",
    infinite: true,
    dot: true,
    slideToShow: 1,
    SlideToScroll: 1,
    resHeight: true
});

$(".carousel2").jqueryCarousel({
    autoScroll: false,
    scrollRight: true,
    infinite: true,
    id: "#sliders-2",
    dot: true,
    slideToShow: 1,
    SlideToScroll: 1,
    resHeight: false
});
