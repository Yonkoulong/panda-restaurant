import React from 'react';
import logo from '../../logo.svg';

const Home = () => {
    return (
        <div>
            <h1 style={{ color: "blue" }}>Welcome to FullSnack Development Nguyễn Giang</h1>
            <img src={logo} alt="logo" className="App-logo" />
            <h2>Edit with ./src/App.js and reload page </h2>
        </div>
    )
}

export default Home
