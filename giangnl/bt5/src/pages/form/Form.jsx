import React, { useState, useEffect } from 'react'
import "./from.css"
import Axios from 'axios'
import { addNewForm } from '../../storge/action/Form';
import { useDispatch, useSelector } from 'react-redux';
import FormView from './FormView';
import { useForm } from 'react-hook-form';


const Form = () => {

  const formData = useSelector(state => state.form.list)
  const dispatch = useDispatch();
  const { register } = useForm();

  const url = `http://localhost:3000/data`;


  const [data, setData] = useState({
    ngchuyen: "",
    sotien: "",
    ngnhan: "",
    date: ""
  })

  const changeValue = (e) => {
    const newData = { ...data }
    newData[e.target.id] = e.target.value
    setData(newData)
    console.log(newData)
  }

  const subMit = (e) => {
    e.preventDefault();
    if (data && data.sotien !== '') {
      Axios.post(url, {
        ngchuyen: data.ngchuyen,
        sotien: data.sotien,
        ngnhan: data.ngnhan,
        date: data.date,
      })
        .then(res => {
          console.log(res.data)
        })

      alert("Submit correct");
      const newData = {
        ngchuyen: "",
        sotien: "",
        ngnhan: "",
        date: ""
      }
      setData(newData)
      handClickFormList()
    }
  }

  const handClickFormList = async () => {
    const res = await Axios.get(url)
    const newList = res.data
    const action = addNewForm(newList)
    dispatch(action)
  }

  const updateItem = async (index) => {
    const update = formData.filter((item) => (
      item.id === index
    ))
    const newData = {
      ngchuyen: update[0].ngchuyen,
      sotien: update[0].sotien,
      ngnhan: update[0].ngnhan,
      date: update[0].date
    }
    setData(newData)
    console.log(data);
    if (newData) {
      await Axios.put(url + "/" + index, {
        ngchuyen: data.ngchuyen,
        sotien: data.sotien,
        ngnhan: data.ngnhan,
        date: data.date,
      })
      handClickFormList();
    }
  }

  const deleteItem = async (index) => {

    let res = await Axios.delete(url + "/" + index)
    console.log(res)
    alert("Delete correct")
    handClickFormList()
  }

  const validate = (index) => {
    console.log(index)
  }


  useEffect(() => {
    handClickFormList();
    return;
  }, [])

  return (
    <div>
      <h1>Form Chuyển tiền API By Developer FullSnack Nguyễn Giang</h1>
      <form className="form-ct" onSubmit={(e) => subMit(e)} method="post">
        <div className="ngchuyen">
          <label htmlFor="ngchuyen">Người Chuyển: </label>
          <input type="text" name="ngchuyen" id="ngchuyen" value={data.ngchuyen}
            placeholder="Người Chuyển" {...register("Người Chuyển", { required: true, maxLength: 80 })} onChange={(e) => changeValue(e)} onBlur={() => validate(this)} />
        </div>
        <div className="sotien">
          <label htmlFor="sotien">Số tiền: </label>
          <input type="text" id="sotien" name="soTien" value={data.sotien} placeholder="Số Tiền" {...register("Số Tiền", { required: true, maxLength: 13 })} onChange={(e) => changeValue(e)} onBlur={() => validate(this)} />
        </div>
        <div className="ngnhan">
          <label htmlFor="ngnhan">Người nhận</label>
          <input type="text" name="ngnhan" id="ngnhan" value={data.ngnhan} placeholder="Người Nhận" onChange={(e) => changeValue(e)} onBlur={() => validate(this)} required />
        </div>
        <div className="date">
          <label id="mes" htmlFor="date">Ngày tháng</label>
          <input type="date" name="date" id="date" value={data.date} onChange={(e) => changeValue(e)} onBlur={() => validate(this)} required />
        </div>
        <button className="bn39">
          <span className="bn39span">Submit</span>
        </button>
      </form>
      {/* <input type="button" onClick={HandClickFormList} value="Get Data Api" /> */}
      <div>
        <FormView
          data={formData}
          isUpdate={updateItem}
          isDelete={deleteItem}
        />
      </div>

    </div>
  )
}

export default Form