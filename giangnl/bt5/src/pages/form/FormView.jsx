import React from 'react'


const FormView = (props) => {
    const { data, isUpdate, isDelete } = props
    console.log(props)

    const handClickDelete = (index) => {
        isDelete(index)
    }

    const handClick = (index) => {
        isUpdate(index)
    }


    return (
        <div>
            <table className="tableForm">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Người Chuyển(Name)</th>
                        <th>Số Tiền(VND)</th>
                        <th>Người Nhận(Name)</th>
                        <th>DATE(Ngày/Tháng/Năm)</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                {/* <FormView /> */}
                <tbody>
                    {data.map(item => {
                        return <tr key={item.id}>
                            <td>{item.id}</td>
                            <td>{item.ngchuyen}</td>
                            <td>{item.sotien}</td>
                            <td>{item.ngnhan}</td>
                            <td>{item.date}</td>
                            <td><button onClick={() => handClick(item.id)} >Update</button><button onClick={() => handClickDelete(item.id)}>Delete</button></td>
                        </tr>
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default FormView