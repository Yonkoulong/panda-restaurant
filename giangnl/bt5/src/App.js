import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Calculator from "./component/calculator/Calculator";
import Todolist from "./component/todoList/Container";
import Nav from "./component/navbar/Nav";
import Form from "./pages/form/Form"
import ErroPager from "./pages/error/Erropage"
import Home from './pages/home/Home'




function App() {
  return (
    <div className="App">
      <Router>
        <Nav />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/Calculator" component={Calculator} />
          <Route path="/Todolist" component={Todolist} />
          <Route path="/form" component={Form} />
          <Route path="/*" component={ErroPager} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
