import React from "react";

import Input from "./Input";

class Container extends React.Component {
    
    render() {
        return (
            <div >
                <h1 style={{color: "blue"}}> My TODOLIST </h1>
                <Input />
            </div>
        )
    }
}

export default Container;
