import React from "react";

class Input extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            todoList: [
                { id: 1, action: "Đi học về", done: false, edit: false },
                { id: 2, action: "Đi chơi", done: false, edit: false },
            ],
            newTodo: "",
        }
    };
    UpdateValue = (event) => {
        this.setState({ newTodo: event.target.value });
    };

    AddItem = (event, index) => {
        let lengItem = this.state.todoList.length - 1
        console.log(lengItem);
        if (lengItem < 0) {
            this.setState({
                todoList: [
                    ...this.state.todoList,
                    { id: lengItem + 2, action: this.state.newTodo, done: false, edit: false }
                ],
                newTodo: "",

            });

        } else if (lengItem >= 0) {
            this.setState({
                todoList: [
                    ...this.state.todoList,
                    { id: this.state.todoList[lengItem].id + 1, action: this.state.newTodo, done: false, edit: false }
                ],
                newTodo: "",

            })

        }



    }

    CheckDone = (todo) => {
        this.setState({
            todoList: this.state.todoList.map((item) =>
                item.action === todo.action ? { ...item, done: !item.done, edit: false } : item
            ),
        });
    }

    DeleteItem = ((ind) => {
        this.setState({
            todoList: this.state.todoList.filter((item, index) =>
                index !== ind
            ),
        });
    })

    UpdateItem = ((todo, index) => {
        console.log(todo.id);

        this.setState({
            newTodo: this.state.todoList[index].action,
            todoList: this.state.todoList.map((item) =>
                todo.id === item.id ? { ...item, edit: !item.edit } : item
            ),
        })
        if (this.state.todoList[index].edit === true) {
            this.setState({
                todoList: this.state.todoList.map((item) =>
                    todo.id === item.id ? { ...item, action: this.state.newTodo, edit: !item.edit } : item
                ),
                newTodo: ""
            });
        }
        console.log(this.state.todoList.filter(todo => todo.edit === true).length)
    })

    render() {

        return (
            <div>
                <div>
                    <input type="text" placeholder="Add Todo" value={this.state.newTodo} onChange={this.UpdateValue} />
                    <button disabled={!this.state.newTodo || this.state.todoList.filter(todo => todo.edit === true).length > 0} onClick={this.AddItem}>Add</button>
                </div>

                <table>
                    <thead>
                        <tr>
                            <th>Công việc định thực hiện : {this.state.newTodo}</th>
                            <th>Tổng số công việc: {(this.state.todoList.length === 0 ? "ĐÃ HẾT VIỆC. PlS ADD JOB" : this.state.todoList.length)} </th>
                        </tr>
                        <tr>
                            <th>Công Việc</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.todoList.map((item, index) => {
                            return <tr key={this.state.todoList[index].id}>
                                <td >{this.state.todoList[index].id + ".  "}
                                    {item.action} {(item.done === true) ? "  Đã xong" : ""}
                                </td>
                                <td>
                                    <button onClick={() => this.CheckDone(item)} >{this.state.todoList[index].done === true ? "Not Done" : "Done"}</button>
                                    <button onClick={() => this.DeleteItem(index)} >Delete</button>
                                    <button onClick={() => this.UpdateItem(item, index)} >{this.state.todoList[index].edit === true ? "Edit" : "Update"}</button>
                                </td>
                            </tr>
                        })}
                    </tbody>
                </table>
            </div >
        );
    };
};

export default Input;