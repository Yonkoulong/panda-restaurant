import React from "react";

class Calculator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            input: "",
            output: [
                { numberOne: '4', numberTwo: '2', operator: 'X', input: 800}
            ],
            numberOne: "",
            numberTwo: "",
            operator: "",
        }
    }

    SetNumber = (event) => {
        let numBerone = this.state.numberOne
        var numBertwo = this.state.numberTwo
        numBerone += event.target.value
        if (this.state.operator.length > 0) {
            numBertwo += event.target.value
            this.setState({ numberTwo: numBertwo });
        } else this.setState({ numberOne: numBerone });

        // console.log(numberFil)
    };

    SetOperator = (event) => {
        this.setState({ operator: event.target.value });
    }

    ClearNumber = (event) => {
        this.setState({ numberOne: "", numberTwo: "", operator: "", input: "" });
    }


    setOutput = (number) => {
        let lengOutput = this.state.output.length - 1;
        if(this.state.output[lengOutput].input === number) {
            return
        }else {
        this.setState({
            output: [
                ...this.state.output,
               { numberOne: this.state.numberOne,numberTwo: this.state.numberTwo , operator: this.state.operator, input: number},
            ]
        })
      }
    }

    SetInput = ((value) => {
        var cong = "";
        var tru = "";
        var nhan = "";
        var chia = "";
        var phantram = "";
        let errorNumber = 1000000000;
        if(this.state.numberOne.includes(".") === true || this.state.numberTwo.includes(".") === true ){
            let errorNumberOne = this.state.numberOne * errorNumber;
            let errorNumberTwo = this.state.numberTwo * errorNumber;
             cong = parseFloat(this.state.numberOne) + parseFloat(this.state.numberTwo);
             tru = parseFloat(this.state.numberOne) - parseFloat(this.state.numberTwo);
             nhan = errorNumberOne * errorNumberTwo / 1000000 / 1000000 / 1000000;
             chia = (parseFloat(this.state.numberOne) / parseFloat(this.state.numberTwo));
             phantram = (this.state.numberOne / 100) * this.state.numberTwo;
        }else {
            cong = parseInt(this.state.numberOne) + parseInt(this.state.numberTwo);
            tru = this.state.numberOne - this.state.numberTwo;
            nhan = this.state.numberOne * this.state.numberTwo;
            chia = this.state.numberOne / this.state.numberTwo;
            phantram = (this.state.numberOne / 100) * this.state.numberTwo;
        }

        console.log(nhan)
        console.log(parseFloat(this.state.numberOne))
        console.log(this.state.numberOne.includes("."))

        if (this.state.operator === ' / ') {
            this.setState((state) => {
                return state.input = chia
            });
            this.setOutput(chia)
        } else if (this.state.operator === ' + ') {
            this.setState((state) => {
                return state.input = cong
            });
            this.setOutput(cong)
        } else if (this.state.operator === ' - ') {

            this.setState((state) => {
                return state.input = tru
            });
            this.setOutput(tru)
        } else if (this.state.operator === ' X ') {

            this.setState((state) => {
                return state.input = nhan
            });
            this.setOutput(nhan)
        }else if (this.state.operator === ' % ') {

            this.setState((state) => {
                return state.input = phantram
            });
            this.setOutput(phantram)
        }
        
    })

    Reload = ((ind) => {
        console.log(this.state.output)
        this.setState({
            output: this.state.output.filter((item,index) => 
                index !== ind
            ),
            numberOne: this.state.output[ind].numberOne,
            numberTwo: this.state.output[ind].numberTwo,
            operator: this.state.output[ind].operator,
            input: this.state.output[ind].input,
        });

    })



    render() {
        return (
            <div className="calculator">
                <h2>Calculator Made By Nguyễn Giang</h2>
                <div className="calculator-main">
                    <p>
                        {this.state.numberOne}{this.state.operator}{this.state.numberTwo}
                    </p>
                    <p>  {this.state.input}</p>
                </div>
                <div>
                    <button className="opsBtn" onClick={this.ClearNumber}>CLEAR</button>
                    <button className="opsBtn" disabled={this.state.output.length <= 0} onClick={() => this.Reload(this.state.output.length -1)}>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M468.9 32.11c13.87 0 27.18 10.77 27.18 27.04v145.9c0 10.59-8.584 19.17-19.17 19.17h-145.7c-16.28 0-27.06-13.32-27.06-27.2c0-6.634 2.461-13.4 7.96-18.9l45.12-45.14c-28.22-23.14-63.85-36.64-101.3-36.64c-88.09 0-159.8 71.69-159.8 159.8S167.8 415.9 255.9 415.9c73.14 0 89.44-38.31 115.1-38.31c18.48 0 31.97 15.04 31.97 31.96c0 35.04-81.59 70.41-147 70.41c-123.4 0-223.9-100.5-223.9-223.9S132.6 32.44 256 32.44c54.6 0 106.2 20.39 146.4 55.26l47.6-47.63C455.5 34.57 462.3 32.11 468.9 32.11z" /></svg></button>
                    <button className="opsBtn" value=' % ' onClick={this.SetOperator}>%</button>
                    <button className="opsBtn" disabled={!this.state.numberOne || this.state.numberTwo.length > 0} value=" / " onClick={this.SetOperator}>/</button>
                </div>
                <div>
                    <button value='7' onClick={this.SetNumber}>7</button>
                    <button value='8' onClick={this.SetNumber}>8</button>
                    <button value='9' onClick={this.SetNumber}>9</button>
                    <button className="opsBtn" disabled={!this.state.numberOne || this.state.numberTwo.length > 0} value=" X " onClick={this.SetOperator}>X</button>
                </div>
                <div>
                    <button value="4" onClick={this.SetNumber}>4</button>
                    <button value='5' onClick={this.SetNumber}>5</button>
                    <button value='6' onClick={this.SetNumber}>6</button>
                    <button className="opsBtn" disabled={!this.state.numberOne || this.state.numberTwo.length > 0} value=" - " onClick={this.SetOperator}>-</button>
                </div>
                <div>
                    <button value='1' onClick={this.SetNumber}>1</button>
                    <button value='2' onClick={this.SetNumber}>2</button>
                    <button value='3' onClick={this.SetNumber}>3</button>
                    <button className="opsBtn" disabled={!this.state.numberOne || this.state.numberTwo.length > 0} value=" + " onClick={this.SetOperator}>+</button>
                </div>
                <div>
                    <button className="btn" value='0' onClick={this.SetNumber}>0</button>
                    <button value='.' onClick={this.SetNumber}>.</button>
                    <button className="opsBtn" disabled={!this.state.numberOne} onClick={this.SetInput} onChange={() => this.displayNumber(this.state.input)}>=</button>
                </div>
            </div>
        )
    }
}

export default Calculator;