import React from "react";
import { Link } from "react-router-dom"

class Nav extends React.Component {

    render() {
        const NavStyle = { color: 'white' };
        return (
            <div className="nav">
                <h3>Logo</h3>
                <ul className="nav-ul">
                    <li >
                        <Link to="/" style={NavStyle}>Home</Link>
                    </li>
                    <li >
                        <Link to="/Calculator" style={NavStyle}>Calculator</Link>
                    </li>
                    <li >
                        <Link to="/Todolist" style={NavStyle}>TodoList</Link>
                    </li>
                    <li >
                        <Link to="/form" style={NavStyle}>Form Monney</Link>
                    </li>
                </ul>
            </div>

        )
    }
}

export default Nav;