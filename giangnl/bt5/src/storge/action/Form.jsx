export const addNewForm = (form) => {
    return {
        type: "ADD_FORM",
        payload: form,
    }
}

export const deleteFromItem = (form) => {
    return {
        type: "DELETE_FORM",
        payload: form,
    }
}

export const updateFromItem = (form) => {
    return {
        type: "UPDATE_FORM",
        payload: form,
    }
}