export const addNewHoppy = (hoppy) => {
    return {
        type: 'ADD_HOPPY',
        payload: hoppy,
    }
}

export const setHoppy = (hoppy) => {
    return {
        type: 'SET_HOPPY',
        payload: hoppy,
    }
}