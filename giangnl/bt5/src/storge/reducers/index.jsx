import { combineReducers } from "redux";
import { UserReducer } from "./User";
import { HoppyReducer } from "./Hoppy"
import { FormReducer } from "./Form";

export const rootReducer = combineReducers({
    hoppy: HoppyReducer,
    user: UserReducer,
    form: FormReducer,
});