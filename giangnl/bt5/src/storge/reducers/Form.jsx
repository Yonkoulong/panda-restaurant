
const inlitallState = {
    list: [],
    delete: "",
    update: "",
}

export const FormReducer = (state = inlitallState, action) => {
    switch (action.type) {
        case "ADD_FORM": {
            // const newList = [...state.list];
            return {
                list: action.payload
            }
        }

        case "REMOVE_FORM": {
            const newDelete = action.payload.id
            return {
                ...state,
                delete: newDelete
            }
        }

        case "UPDATE_FORM": {
            const newUpdate = action.payload.id
            return {
                ...state,
                update: newUpdate
            }
        }

        default:
            return state;
    }
}