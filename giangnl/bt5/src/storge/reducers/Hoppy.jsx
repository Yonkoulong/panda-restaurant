
const inlitallState = {
    list: [],
    activeId: null,
}

export const HoppyReducer = (state = inlitallState, action) => {
    switch (action.type) {
        case 'ADD_HOPPY': {
            const newList = [...state.list];
            return {
                ...state.list,
                list: newList.push(action.payload),
            };
        }
        case 'SET_HOPPY': {
            const newActive = action.payload.id
            return {
                ...state,
                activeId: newActive,
            };
        }
        default:
            return state;
    }

}